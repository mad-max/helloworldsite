<?php

/*
 * Включить необходимые файлы
 * init генерирует маркер защиты от CSRF 'token'
 * загружает конфигурационную информацию из файла конфигурации
 * подключается к базе данных
 */
include_once '../sys/core/init.inc.php'; 

/*
 * Задать название страницы и файлы CSS
 * переменная и массив обрабатывается в headerHWS.inc.php
 */
$page_title="My &laquo;HelloWorld&raquo; Site!";
$css_files=array('login.css', 'normalize.css' );

/*
 * Включить начальную часть страницы
 */
include_once './assets/common/header.inc.php';

?>




<div id="centerLayerLogin">
    <h1>Hello, World!</h1>
    <p>Это мой первый сайт, так сказать, &laquo;HelloWorld&raquo; в веб-разработке. Для начала необходимо залогиниться.</p>
    <form action="assets/inc/process.inc.php"
        method="post">
        <fieldset id="fieldsetLoginHws" title="Введите логин и пароль, затем нажмите &laquo;Отправить&raquo;.">
            <div>
                <input class="loginInput" type="text" maxlength="48"
                    name="userLogin"
                    value="Login"/>
            </div>
            <div>
                <input class="pwdInput" type="password" maxlength="48"
                    name="userPassword"
                    value="Password"/>
            </div>
            <div>
                <input class="chckInput" type="checkbox" id="chck1"
                    name="rememberMe"
                    value=""/>
                <label for="chck1"><span class="chckInput">Запомнить</span></label>
            </div>
            <div>
                <input type="hidden" name="token"   
                    value="<?php echo $_SESSION['token']; ?>" />    <!--храним токен, проверяем его в processHWS.inc.php-->
                <input type="hidden" name="action"  
                    value="user_login" />   <!--храним действие которое надо выполнить в processHWS.inc.php -->
            </div>
            <div>
                <input class="buttonInput" type="submit"
                    value="Отправить"/>
            </div>
	</fieldset>
    </form>
</div>



<?php

/*
 * Включить завершающую часть страницы
 */
include_once './assets/common/footer.inc.php';



