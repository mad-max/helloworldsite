<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Включить необходимые файлы
 */
include_once '../sys/core/init.inc.php';    //Данный файл init.inc.php генерирует маркер защиты от CSRF 'token', загружает конфигурационную информацию из файла конфигурации, подключается к базе данных.
    
/*
* Перенапрвить незарегистрированного пользователя на
* основную страницу
*/
if (!isset($_SESSION['user'])) {
    header("Location: ./index.php");
    exit;
}
    
/*
 * Вывести начальную часть страницы
 */
$page_title="&laquo;HelloWorld!&raquo; site";
$css_files=array('main.css', 'normalize.css', 'global.css', 'edit.css', 'calstyle.css');
include_once 'assets/common/header.inc.php';


//filter_input — Принимает переменную извне PHP и, при необходимости, фильтрует ее
$id=filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

/*
 * Убедиться в том, что ID был передан  и пользователь выполнил вход
 */
if (empty($id)) {
    /*
     * Если ID не был предоставлен вернуть пользователя
     * на основную страницу
     */
    header("Location: /main.php");
    exit;
}
    

//$article=array();
$article=Article::getById($id);

/*
 * Проверим ID в объекте статья. Если его нет значит статья удалена.
 */
if (empty ($article->id)) {
    
    die ("ID is null");
}
//print_r ($article);


/*
 * Загрузить календарь
 */
$date=date ('Y-m-d H:i:s');
$cal=new Calendar($dbo, $date);

$calendar=$cal->buildCalendar();
 

//Создаем ассоциативный массив из массива с объектом "статья"
$test=array ("article"=>$article, "cal"=>$calendar);
    
//Создаем объект "шаблона"
$template = new Template("assets/templates/");  //путь к папке с шаблонами. должен заканчиваться /
    
//Устанавливаем свойтсва объекта
$template->set("test", $test);  //устанавливаем тестовую строку
    
//Выводим шаблон
$template->display("deleteArticle");     //имя шаблона













/*
 * Вывести завершающую часть страницы
 */
include_once 'assets/common/footer.inc.php';
