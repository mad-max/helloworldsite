<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * Данный файл init.inc.php генерирует маркер защиты от CSRF 'token',
 * загружает конфигурационную информацию из файла конфигурации,
 * подключается к базе данных.
 */
include_once '../sys/core/init.inc.php';    

/*
* Перенапрвить незарегистрированного пользователя на
* основную страницу
*/
if (!isset($_SESSION['user'])) {
    header("Location: ./index.php");
    exit;
}
    
/*
 * Вывести начальную часть страницы
 */
$page_title="&laquo;HelloWorld!&raquo; site";
$css_files=array('main.css', 'normalize.css', 'global.css', 'calstyle.css', 'edit.css');
include_once 'assets/common/header.inc.php';
    
    
//filter_input — Принимает переменную извне PHP и, при необходимости, фильтрует ее
$id=filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);


//print "get<br>";
//print_r ($_GET);
//print "<br>";
//print "post<br>";
//print_r ($_POST);

/*
 * Получаем номер требуемого смещения для вывода комментариев из БД из GET запроса
 * intval - получает целочисленное значение переменной.
 */
$start=isset($_GET['page']) ? intval ($_GET['page']) : 0;
$page=filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);

/*
 * Создаем объект Статья
 */
$article=Article::getById($id);


/*
 * Обрезаем текст до нужного количества символов
 */
//$article->author=$article->get_cut_text($article->author, 50);
//echo $article->author;
//$article->category=$article->get_cut_text($article->category, 50);
//$article->editDate=$article->get_cut_text($article->editDate, 30);
//$article->img_src=$article->get_cut_text($article->img_src, 50);
//$article->publicationDate=$article->get_cut_text($article->publicationDate, 30);
$article->summary=$article->get_cut_text($article->summary, $C['summaryLength']);
$article->title=$article->get_cut_text($article->title, $C['titleLength']);



/*
 * Заменяем bbcode на html теги
 */
$bbContent=$article->replaceBBCode($article->content);


/*
 * Проверим ID в объекте статья. Если его нет значит статья удалена.
 */
if (empty ($article->id)) {
    
    die ("ID is null");
}


/*
 * Получить категорию текущей статьи
 */
$activeCategory=ext_db::getActiveCategory ($article->category);


/*
 * Загрузить календарь
 */
$date=date ('Y-m-d H:i:s');
$cal=new Calendar($dbo, $date);

$calendar=$cal->buildCalendar();
    
/*
 * Кол-во выводимых комментариев на странице
 * Кол-во ссылок в нумерации страниц
 */
$limit=3;
$linkLimit=5;

/*
 * Загрузить комментарии.
 * Узнаем кол-во комментариев.
 */
$commentList=Comment::getByArticle($id, $dbo, $start, $limit);

//print_r ($commentList);

/*
 * Получаем кол-во всех комментариев в БД
 */
$all=$commentList['totalRows']['totalRows'];


/*
 * Заменяем bbcode на html теги в свойствах объектов из которых состоит массив комментариев.
 * Т. к. нам не надо сохранять bbcode, то меняем именно свойства самих объектов.
 * (например в editArticle создавали отдельный элемент bbContent в массиве test).
 * Обрезаем текст до нужного кол-ва символов
 * 
 */
foreach ($commentList['results'] as $key => $val) {
    $val->comment=$val->get_cut_text($val->comment, $C['commentLength']);
    $val->comment=$val->replaceBBCode($val->comment);
}


    
/*
 * Создаем объект "пагинатор".
 * Указываем сколько комментариев показывать на одной странице и
 * сколько ссыллок на страницы показываеть под комментариями.
 */
$pageNav=new pageNav($all, $limit, $linkLimit);
    
/*
 * Создаем пагинацию для требуемой страницы
 */
$htmlNav=$pageNav->getNavLinks($start);


/*
 * Создаем ассоциативный массив из массива с объектом "статья"
 */
$test=array ("article"=>$article, "bbContent"=>$bbContent, 
    "activeCategory"=>$activeCategory, "cal"=>$calendar,
    "comments"=>$commentList['results'], "commentCount"=>$commentList['totalRows'], 
    "htmlNav"=>$htmlNav, "currentPage"=>$page);


/*
 * Проверяем разрешения пользователя. Редактирование статьи.
 */
$test['editPerm']=Role::hasPrivilege($_SESSION['user']['id'], 'edit any article');
if ($test['editPerm']===false and $article->author===$_SESSION['user']['name'])  {
    $test['editPerm']=Role::hasPrivilege($_SESSION['user']['id'], 'edit own article');
}

/*
 * Проверяем разрешения пользователя. Удаление статьи.
 */
$test['deletePerm']=Role::hasPrivilege($_SESSION['user']['id'], 'delete any article');
if ($test['deletePerm']===false and $article->author===$_SESSION['user']['name'])  {
    $test['deletePerm']=Role::hasPrivilege($_SESSION['user']['id'], 'delete own article');
}

/*
 * Добавляем в массив элемент разрешения с индексом editCom. 
 * Проверяем разрешения пользователя. Редактирование комменатрия.
 */
$test['editCom']=Role::hasPrivilege($_SESSION['user']['id'], 'edit own comment');


/*
 * Добавляем в массив элемент разрешения с индексом deleteCom. 
 * Проверяем разрешения пользователя. Удаление комменатрия.
 */
$test['deleteCom']=Role::hasPrivilege($_SESSION['user']['id'], 'delete own comment');


/*
 * Добавляем в массив элемент разрешения с индексом editCom. 
 * Проверяем разрешения пользователя. Редактирование комменатрия.
 */
$test['editAnyCom']=Role::hasPrivilege($_SESSION['user']['id'], 'edit any comment');


/*
 * Добавляем в массив элемент разрешения с индексом deleteCom. 
 * Проверяем разрешения пользователя. Удаление комменатрия.
 */
$test['deleteAnyCom']=Role::hasPrivilege($_SESSION['user']['id'], 'delete any comment');


/*
 * Добавляем в массив элемент разрешения с индексом addCom. 
 * Проверяем разрешения пользователя. Добавление статьи.
 */
$test['addCom']=Role::hasPrivilege($_SESSION['user']['id'], 'add comment');




//Создаем объект "шаблона"
$template = new Template("assets/templates/");  //путь к папке с шаблонами. должен заканчиваться /
    
//Устанавливаем свойтсва объекта
$template->set("test", $test);  //устанавливаем тестовую строку
    
//Выводим шаблон
$template->display("article");     //имя шаблона


/*
 * Вывести завершающую часть страницы
 */
include_once 'assets/common/footer.inc.php';

