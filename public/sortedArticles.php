<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




    
    /*
     * Включить необходимые файлы
     */
    include_once '../sys/core/init.inc.php';    //Данный файл init.inc.php генерирует маркер защиты от CSRF 'token', загружает конфигурационную информацию из файла конфигурации, подключается к базе данных.
    
    /*
    * Перенапрвить незарегистрированного пользователя на
    * основную страницу
    */
    if (!isset($_SESSION['user'])) {
        header("Location: ./index.php");
        exit;
    }
    
    /*
     * Вывести начальную часть страницы
     */
    $page_title="&laquo;HelloWorld!&raquo; site";
    $css_files=array('main.css', 'normalize.css', 'global.css', 'calstyle.css');
    include_once 'assets/common/header.inc.php';
    
    $limit=3;
    $linkLimit=5;
    
    /*
     * Получаем номер требуемого смещения для БД из GET запроса
     * intval - получает целочисленное значение переменной.
     */
    $start=isset($_GET['page']) ? intval ($_GET['page']) : 0;
    
    /*
     * Получаем требуемую категорию из get запроса
     */
    $category=isset($_GET['category']) ? filter_input(INPUT_GET, 'category', FILTER_SANITIZE_SPECIAL_CHARS) : NULL;
    
    
    
    /*
     * Получаем массив с количестом "limit" объектов "статья" начиная от требуемого смещения
     */
    $article=Article::getListByCategory($start, $limit, $category, "publicationDate DESC");
    
    /*
     * Получаем кол-во всех статей в БД
     */
    $all=$article['totalRows']['totalRows'];
    
    /*
     * Заменяем bbcode на html теги в свойствах объектов из которых состоит массив $article['results'].
     * Т. к. нам не надо сохранять bbcode, то меняем именно свойства самих объектов.
     * (например в editArticle создавали отдельный элемент bbContent в массиве test).
     * Обрезаем текст до нужного кол-ва символов
     */
    foreach ($article['results'] as $key => $val) {
        $val->title=$val->get_cut_text($val->title, $C['titleLength']);
        $val->content=$val->get_cut_text($val->content, $C['previewLength']);
        $val->content=$val->replaceBBCode($val->content);
        // Очистка от тегов <img> 
        //$val->content = preg_replace('/<img [^>]*? >/xis','', $val->content);
        
    }
    
    
    
    /*
     * Создаем объект "пагинатор"
     */
    $pageNav=new pageNav($all, $limit, $linkLimit);
    
    /*
     * Создаем пагинацию для требуемой страницы с указанием категории требуемых статей
     */
    $htmlNav=$pageNav->getNavLinks($start, $category);
    
    
/*
 * Загрузить календарь
 */
$date=date ('Y-m-d H:i:s');
$cal=new Calendar($dbo, $date);

$calendar=$cal->buildCalendar();
  
    
    
    /*
     * Создаем ассоциативный массив из объектов "статья", "пагинатор" для шаблона
     */
    $test=array ("articles"=>$article['results'], "htmlNav"=>$htmlNav, "cal"=>$calendar );
    
        
    /*
     * Добавляем в массив элемент разрешения с индексом addArticlePerm. 
     * Проверяем разрешения пользователя. Добавление статьи.
     */
    $test['addArticlePerm']=Role::hasPrivilege($_SESSION['user']['id'], 'add article');
    
     
    
    /*
     * Создаем объект "шаблона"
     */
    $template = new Template("assets/templates/");  //путь к папке с шаблонами. должен заканчиваться /
    
    /*
     * Устанавливаем свойтсва объекта
     */
    $template->set("test", $test);  //устанавливаем тестовую строку
    
    /*
     * Выводим шаблон
     */
    $template->display("sortedArticles");     //имя шаблона
    

/*
 * Вывести завершающую часть страницы
 */
include_once 'assets/common/footer.inc.php';