<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Запуск сеанса
 */
session_start();

/*
 * Включить необходимые файлы. Т.к. в этом скрипте будут использоваться классы которым они необходимы
 */
include_once '../../../sys/config/db_cred.inc.php';

/*
 * Определить константы для конфигурационной информации
 */
foreach ($C as $name=>$val) {
    define($name, $val);
}

$page=filter_input (INPUT_POST, 'page', FILTER_SANITIZE_NUMBER_INT);
$articleId=filter_input (INPUT_POST, 'articleID', FILTER_SANITIZE_NUMBER_INT);
$id=filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

/*
 * Создать поисковый массив для действий, выполняемых над формой
 */
$actions=array(
    'user_login'=>array(    
        'object'=>'Admission',
        'method'=>'processLoginForm',
        'header'=>'Location: /main.php'
        ),
    'user_logout'=>array(
        'object'=>'Admission',
        'method'=>'processLogout',
        'header'=>'Location: /index.php'
    ),
    'article_edit'=>array(
        'object'=>'Article',
        'method'=>'saveArticle',
        'header'=>'Location: ../../article.php?id='.$id
    ),
    'article_delete'=>array(
        'object'=>'Article',
        'method'=>'delete',
        'header'=>'Location: /main.php'
    ),
    'article_new'=>array(
        'object'=>'Article',
        'method'=>'saveArticle',
        'header'=>'Location: /main.php'
    ),
    'addComment'=>array(
        'object'=>'Comment',
        'method'=>'addComment',
        'header'=>'Location: /article.php?id='.$articleId.'&page='.$page
    ),
    'comment_delete'=>array(
        'object'=>'Comment',
        'method'=>'deleteComment',
        'header'=>'Location: /article.php?id='.$articleId.'&page='.$page
    ),
    'comment_edit'=>array(
        'object'=>'Comment',
        'method'=>'editComment',
        'header'=>'Location: /article.php?id='.$articleId.'&page='.$page
    )
    
);  //$actions array



/*
 * Убедиться в том, что маркер защиты от CSRF был передан и что
 * запрошенное действие существует в поисковом массиве
 */
if ($_POST['token']==$_SESSION['token'] && isset($actions[$_POST['action']])) { //токен генерируется в init.inc.php. action передается из скрытой формы метода displayForm класса календарь страницы админ.пхп
    $use_array=$actions[$_POST['action']]; 
    $obj=new $use_array['object']($dbo);    
    if (TRUE===$msg=$obj->$use_array['method']($dbo)) { 
        header($use_array['header']);   //перенаправление
        exit;
    } else {
        //В случае ошибки вывести сообщение о ней и прекратить выполнение
        die($msg);
        
    }
} else {
    //В случае некорректности маркера/действия перенаправить
    //пользователя на основную страницу
    header("Location: /index.php"); //проверено
    exit;
}

/*
 * Функция автозагрузки
 */
function __autoload($class_name) {
    $filename='../../../sys/class/class.'.strtolower($class_name).'.inc.php';
    if (file_exists($filename)) {
        include_once $filename;
    }
}