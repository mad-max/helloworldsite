<div id="layout">
    <div id="navbar">
        <div class="navPanel">
            navPanel
        </div>
    </div>
    <div class="inner">
        <div class="content">
            <div class="siteName">
                <h1><a class="siteName" href="./main.php"><?php echo siteName; ?></a></h1>
            </div>
            <table class="menu">
                <tbody>
                    <tr>
                        <td class="item">
                            <a href="./main.php">
                                <span class="./main.php"><?php echo topMenu1SortedArticles; ?></span>
                            </a>
                        </td>
                        <td class="item<?php if ($_GET['category']===topMenu2SortedArticles) { echo ' active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu2SortedArticles); ?>">
                                <span class="name"><?php echo topMenu2SortedArticles; ?></span>
                            </a>
                        </td>
                        <td class="item <?php if ($_GET['category']===topMenu3SortedArticles) { echo 'active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu3SortedArticles); ?>">
                                <span class="name"><?php echo topMenu3SortedArticles; ?></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="postList">
                <div class="posts">
                    
                    <!-- Cтатья -->
                    
                    <div id="post_<?php echo $this->test['article']->id; ?>" class="post">
                        <div class="title">
                            <h2>
                                <a class="post_title" href="./article.php?id=<?php echo $this->test['article']->id ?>"><?php echo $this->test['article']->title; ?></a>
                            </h2>
                        </div>
                        <div class="published">
                            <?php echo (date('j F Y H:i', $this->test['article']->publicationDate)); ?>
                        </div>
                        <div class="content_html">
                            <?php if (isset($this->test['article']->img_src)) { ?>
                                <img align="left" src="<?php echo $this->test['article']->img_src; ?>"/>
                            <?php } ?>
                            <?php echo $this->test['bbContent']; ?>
                        </div>
                        <div class="published">
                            Дата редактирования: <?php if (!empty($this->test['article']->editDate)) { echo (date('j F Y H:i', $this->test['article']->editDate));} ?>
                        </div>
                        <div class="published">
                            Автор: <?php echo $this->test['article']->author; ?>
                        </div>
                    </div>


                    <!-- Редактирование статьи -->
                    <form action="assets/inc/process.inc.php" method="post">
                        <fieldset>
                            <legend>Редактирование статьи</legend>

                            <label for="article_title">Заголовок</label>
                            <input type="text" name="title"
                                id="article_title" value="<?php echo $this->test['article']->title; ?>" />

                            <label for="article_summary">Краткое содержание</label>
                            <input type="text" name="summary"
                                id="article_summary" value="<?php echo $this->test['article']->summary; ?>" />
                            
                            <label for="article_description">Статья</label>
                            <div id="bbButtons">
                                <input id="quote-button" class="bbButton" type="button"  value="Цитата"  />
                                <input id="code-button" class="bbButton" type="button"  value="Код" />
                                <input id="bold-button" class="bbButton" type="button"  value="Ж" />
                                <input id="italic-button" class="bbButton" type="button"  value="К" />
                                <input id="underline-button" class="bbButton" type="button"  value="Ч" />
                                <input id="strike-button" class="bbButton" type="button"  value="П" />
                                <input id="url-button" class="bbButton" type="button"  value="Ссылка" />
                                <input id="img-button" class="bbButton" type="button"  value="Рисунок" />
                                <input id="video-button" class="bbButton" type="button"  value="Видео" />
                                <input id="size-button" class="bbButton" type="button"  value="Размер" />
                                <input id="color-button" class="bbButton" type="button"  value="Цвет" />
                                <input id="list-button" class="bbButton" type="button"  value="" />
                                <input id="listn-button" class="bbButton" type="button"  value="" />
                                <input id="listm-button" class="bbButton" type="button"  value="Пункт списка" />
                            </div>
                            <textarea name="content"
                                id="article_description"><?php echo $this->test['article']->content; ?></textarea>

                            <label for="article_img_src">Изображение</label>
                            <input type="text" name="img_src"
                                id="article_img_src" value="<?php echo $this->test['article']->img_src; ?>" />
                            
                             
                                
                            <label for="article_category">Категория</label>
                            <select name="category" id="article_category">
                                <option selected value="<?php echo $this->test['activeCategory']['id_category']; ?>"><?php echo $this->test['activeCategory']['name_category']; ?></option>
                                <?php foreach ($this->test['listCategory'] as $key=>$val) { ?>
                                    <option value="<?php echo $val['id_category']; ?>"><?php echo $val['name_category']; ?></option>
                                <?php } ?>
                            </select>
                            <br />
                            <br />
                            <input type="hidden" name="id" value="<?php echo $this->test['article']->id; ?>" />
                            <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                            <input type="hidden" name="action" value="article_edit" />
                            <input type="submit" name="article_submit" value="Отправить" />
                                 <a class="abort" href="./article.php?id=<?php echo $this->test['article']->id ?>">Отменить</a>
                        </fieldset>
                    </form>
                    
                </div> <!-- Редактирование статей -->
                
            </div>
        </div>  
        <div class="sidebarRight">
            <div id="calendar" class="block">
                <?php echo $this->test['cal']; ?>
            </div>
            <div class="block user">
                <div class="title">
                    Hello, <?php  echo ($_SESSION['user']['name']); ?>!<br />
                </div>
                <div class="userList">
                    Your ID is <?php  echo ($_SESSION['user']['id']); ?>.<br />
                    Your E-mail is <?php  echo ($_SESSION['user']['email']); ?>.<br />
                    Your IP is <?php  echo ($_SERVER['REMOTE_ADDR']); ?>.<br />
                    <form method="post" action="assets/inc/process.inc.php">
                        <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                        <input type="hidden" name="action" value="user_logout" />
                        <input type="submit" value="Выход"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="footerPanel">
            Copyright © HelloWorldSite
        </div>
    </div>    
</div> <!--end #content-->