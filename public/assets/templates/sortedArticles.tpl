
<div id="layout">
    <div id="navbar">
        <div class="navPanel">
            navPanel
        </div>
    </div>
    <div class="inner">
        <div class="content">
            <div class="siteName">
                <h1><a class="siteName" href="./main.php"><?php echo siteName; ?></a></h1>
            </div>
            <table class="menu">
                <tbody>
                    <tr>
                        <td class="item">
                            <a href="./main.php">
                                <span class="name"><?php echo topMenu1SortedArticles; ?></span>
                            </a>
                        </td>
                        <td class="item <?php if ($_GET['category']===topMenu2SortedArticles) { echo 'active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu2SortedArticles); ?>">
                                <span class="name"><?php echo topMenu2SortedArticles; ?></span>
                            </a>
                            
                        </td>
                        <td class="item <?php if ($_GET['category']===topMenu3SortedArticles) { echo 'active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu3SortedArticles); ?>">
                                <span class="name"><?php echo topMenu3SortedArticles; ?></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="postList">
                <div class="posts">
                    <!-- Список статей -->
                    <?php foreach ($this->test['articles'] as $key => $val) { ?>
                        <div id="post_<?php echo $val->id; ?>" class="post">
                            <div class="title">
                                <h2>
                                    <a class="post_title" href="./article.php?id=<?php echo $val->id ?>"><?php echo $val->title; ?></a>
                                </h2>
                            </div>
                            <div class="published">
                                <?php echo (date('j F Y H:i', $val->publicationDate)); ?>
                            </div>
                            <div class="content_html">
                                <?php if (isset($val->img_src)) { ?>
                                    <img align="left" src="<?php echo $val->img_src; ?>"/>
                                <?php } ?>
                                <?php echo $val->content; ?>
                            </div>
                            <div class="content_html">
                                <?php $activeCategory=ext_db::getActiveCategory ($val->category); ?>
                                Категория: <?php echo $activeCategory['name_category']; ?>
                            </div>
                        </div>
                    <?php } ?> <!-- end foreach -->
                    
                    <!-- Проверка разрешений -->
                    <?php if ($this->test['addArticlePerm']===true) { ?>
                        <div class="admin_tools">
                            <a class="admin_link" href="./newArticle.php">Добавить новую статью</a>
                        </div>
                    <?php } ?>
                    
                </div> <!-- Список статей -->
                <div class="pageNav">
                    
                    <ul id="navPages">
                        <!-- Формируем ссылки "в начало" и "предыдущая" -->
                        <?php $numberPage=$this->test['htmlNav']['htmlCurrentPage'];
                        if ($numberPage==1) { ?>
                            <li>
                                <span>В начало</span>
                            </li>
                            <li>
                                <span>Предыдущая</span>
                            </li>
                        <?php } else { ?>
                            <li>
                                <a href="<?php echo $this->test['htmlNav']['htmlNavStart'];?>">В начало</a>
                            </li>
                            <li>
                                <a href="<?php echo $this->test['htmlNav']['htmlNavPrev'];?>">Предыдущая</a>
                            </li>
                        <?php } ?>
                        
                        <!-- Формируем ссылки c номерами страниц -->    
                        <?php foreach ($this->test['htmlNav']['htmlOut'] as $key => $val) { 
                            if ($numberPage==$key) { ?>
                                <li>
                                    <span><?php echo $key;?></span>
                                </li>
                            <?php } else { ?>
                                <li>
                                    <a href="<?php echo $val;?> "><?php echo $key;?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        
                        <!-- Формируем ссылки "следующая" и "в конец" -->
                        <?php if ($numberPage==$this->test['htmlNav']['htmlLastPage']) { ?>
                            <li>
                                <span>Следующая</span>
                            </li>
                            <li>
                                <span>В конец</span>
                            </li>
                        <?php } else { ?>
                            <li>
                                <a href="<?php echo $this->test['htmlNav']['htmlNavNext'];?>">Следующая</a>
                            </li>
                            <li>
                                <a href="<?php echo $this->test['htmlNav']['htmlNavEnd'];?>">В конец</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>  
        <div class="sidebarRight">
            <div id="calendar" class="block">
                <?php echo $this->test['cal']; ?>
            </div>
            <div class="block user">
                <div class="title">
                    Hello, <?php  echo ($_SESSION['user']['name']); ?>!<br />
                </div>
                <div class="userList">
                    Your ID is <?php  echo ($_SESSION['user']['id']); ?>.<br />
                    Your E-mail is <?php  echo ($_SESSION['user']['email']); ?>.<br />
                    Your IP is <?php  echo ($_SERVER['REMOTE_ADDR']); ?>.<br />
                    <form method="post" action="assets/inc/process.inc.php">
                        <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                        <input type="hidden" name="action" value="user_logout" />
                        <input type="submit" value="Выход"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="footerPanel">
            Copyright © HelloWorldSite
        </div>
    </div>    
</div> <!--end #content-->