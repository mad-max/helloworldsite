<div id="layout">
    <div id="navbar">
        <div class="navPanel">
            navPanel
        </div>
    </div>
    <div class="inner">
        <div class="content">
            <div class="siteName">
                <h1><a class="siteName" href="./main.php"><?php echo siteName; ?></a></h1>
            </div>
            <table class="menu">
                <tbody>
                    <tr>
                        <td class="item">
                            <a href="./main.php">
                                <span class="./main.php"><?php echo topMenu1SortedArticles; ?></span>
                            </a>
                        </td>
                        <td class="item<?php if ($_GET['category']===topMenu2SortedArticles) { echo ' active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu2SortedArticles); ?>">
                                <span class="name"><?php echo topMenu2SortedArticles; ?></span>
                            </a>
                        </td>
                        <td class="item <?php if ($_GET['category']===topMenu3SortedArticles) { echo 'active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu3SortedArticles); ?>">
                                <span class="name"><?php echo topMenu3SortedArticles; ?></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="postList">
                <div class="posts">
                    
                    <!-- Cтатья -->
                    <legend>Удалить статью?</legend>
                    
                    <div id="post_<?php echo $this->test['article']->id; ?>" class="post">
                        <div class="title">
                            <h2>
                                <a class="post_title" href="./article.php?id=<?php echo $this->test['article']->id ?>"><?php echo $this->test['article']->title; ?></a>
                            </h2>
                        </div>

                        <div class="published">
                            Дата публикации: <?php echo (date('j F Y H:i', $this->test['article']->publicationDate)); ?>
                        </div>

                        <div class="published">
                            Дата редактирования: <?php if (!empty($this->test['article']->editDate)) { echo (date('j F Y H:i', $this->test['article']->editDate)); } ?>
                        </div>

                        <div class="published">
                            Автор: <?php echo $this->test['article']->author; ?>
                        </div>
                    </div>
                    
                    
                    <!-- Редактирование статьи -->
                    <form action="assets/inc/process.inc.php" method="post">
                        <fieldset>                           
                            <input type="hidden" name="id" value="<?php echo $this->test['article']->id; ?>" />
                            <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                            <input type="hidden" name="action" value="article_delete" />
                            <input type="submit" name="article_submit" value="Удалить" />
                                 <a class="abort" href="./article.php?id=<?php echo $this->test['article']->id ?>">Отменить</a>
                        </fieldset>
                    </form>
                </div> <!-- Редактирование статей -->
                
            </div>
        </div>  
        <div class="sidebarRight">
            <div id="calendar" class="block">
                <?php echo $this->test['cal']; ?>
            </div>
            <div class="block user">
                <div class="title">
                    Hello, <?php  echo ($_SESSION['user']['name']); ?>!<br />
                </div>
                <div class="userList">
                    Your ID is <?php  echo ($_SESSION['user']['id']); ?>.<br />
                    Your E-mail is <?php  echo ($_SESSION['user']['email']); ?>.<br />
                    Your IP is <?php  echo ($_SERVER['REMOTE_ADDR']); ?>.<br />
                    <form method="post" action="assets/inc/process.inc.php">
                        <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                        <input type="hidden" name="action" value="user_logout" />
                        <input type="submit" value="Выход"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="footerPanel">
            Copyright © HelloWorldSite
        </div>
    </div>    
</div> <!--end #content-->