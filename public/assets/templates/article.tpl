
<div id="layout">
    <div id="navbar">
        <div class="navPanel">
            navPanel
        </div>
    </div>
    <div class="inner">
        <div class="content">
            <div class="siteName">
                <h1><a class="siteName" href="./main.php"><?php echo siteName; ?></a></h1>
            </div>
            <table class="menu">
                <tbody>
                    <tr>
                        <td class="item">
                            <a href="./main.php">
                                <span class="./main.php"><?php echo topMenu1SortedArticles; ?></span>
                            </a>
                        </td>
                        <td class="item<?php if ($_GET['category']===topMenu2SortedArticles) { echo ' active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu2SortedArticles); ?>">
                                <span class="name"><?php echo topMenu2SortedArticles; ?></span>
                            </a>
                        </td>
                        <td class="item <?php if ($_GET['category']===topMenu3SortedArticles) { echo 'active';} ?>">
                            <a href="./sortedArticles.php?category=<?php echo $var=urlencode(topMenu3SortedArticles); ?>">
                                <span class="name"><?php echo topMenu3SortedArticles; ?></span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="postList">
                <div class="posts">
                    <!-- Список статей -->
                    
                    <div id="post_<?php echo $this->test['article']->id; ?>" class="post">
                        <div class="title">
                            <h2>
                                <a class="post_title" href="./article.php?id=<?php echo $this->test['article']->id; ?>"><?php echo $this->test['article']->title; ?></a>
                            </h2>
                        </div>
                        <div class="published">
                            <?php echo (date('j F Y H:i', $this->test['article']->publicationDate)); ?>
                        </div>
                        <div class="content_html">
                            <?php if (isset($this->test['article']->img_src)) { ?>
                                <img align="left" src="<?php echo $this->test['article']->img_src; ?>"/>
                            <?php } ?>
                            <?php echo $this->test['bbContent']; ?>
                        </div>
                        <div class="content_html">
                            Автор: <?php echo $this->test['article']->author; ?>
                        </div>
                        <div class="content_html">
                            Категория: <?php echo $this->test['activeCategory']['name_category']; ?>
                        </div>
                    </div>
                    
                    <!-- Проверка разрешений -->
                    <?php if ($this->test['editPerm']===true) { ?>
                        <div id="editArticle" class="admin_tools">
                            <a class="admin_link" href="./editArticle.php?id=<?php echo $this->test['article']->id; ?>">Редактировать</a>
                        </div>
                    <?php } ?>
                    
                    <!-- Проверка разрешений -->
                    <?php if ($this->test['deletePerm']===true) { ?>
                        <div id="deleteArticle" class="admin_tools">
                            <a class="admin_link" href="./deleteArticle.php?id=<?php echo $this->test['article']->id; ?>">Удалить</a>
                        </div>
                    <?php } ?>
                    <br />
                    <br />
                    
                    <?php if ($this->test['addCom']===true) { ?>
                        <!-- Комментирование -->
                        <form action="assets/inc/process.inc.php" method="post">
                            <fieldset>
                                <label for="comment">Ваш комментарий</label>
                                <div id="bbButtons">
                                    <input id="quote-button" class="bbButton" type="button"  value="Цитата"  />
                                    <input id="code-button" class="bbButton" type="button"  value="Код" />
                                    <input id="bold-button" class="bbButton" type="button"  value="Ж" />
                                    <input id="italic-button" class="bbButton" type="button"  value="К" />
                                    <input id="underline-button" class="bbButton" type="button"  value="Ч" />
                                    <input id="strike-button" class="bbButton" type="button"  value="П" />
                                    <input id="url-button" class="bbButton" type="button"  value="Ссылка" />
                                    <input id="img-button" class="bbButton" type="button"  value="Рисунок" />
                                    <input id="video-button" class="bbButton" type="button"  value="Видео" />
                                    <input id="size-button" class="bbButton" type="button"  value="Размер" />
                                    <input id="color-button" class="bbButton" type="button"  value="Цвет" />
                                    <input id="list-button" class="bbButton" type="button"  value="" />
                                    <input id="listn-button" class="bbButton" type="button"  value="" />
                                    <input id="listm-button" class="bbButton" type="button"  value="Пункт списка" />
                                </div>
                                <textarea name="comment"
                                    id="article_description"></textarea>
                                <br />
                                <br />
                                <input type="hidden" name="page" value="<?php echo $this->test['currentPage']; ?>" />
                                <input type="hidden" name="articleID" value="<?php echo $this->test['article']->id; ?>" />
                                <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                                <input type="hidden" name="userName" value="<?php  echo ($_SESSION['user']['name']); ?>" />
                                <input type="hidden" name="action" value="addComment" />
                                <input type="submit" name="comment_submit" value="Комментировать" />
                            </fieldset>
                        </form>
                    <?php } ?> <!-- end if -->
                    
                    <!-- Comments -->
                    <div id="comments" class="comments_list">
                        <div id="coments_count" class="coments_count">
                            Комментарии (<?php echo $this->test['commentCount']['totalRows'] ?>)
                        </div>
                        <?php foreach ($this->test['comments'] as $key => $val) { ?>
                            <div id="comment_<?php echo $val->id ?>" class="comment">
                                <div class="commentAuthor">
                                    <?php echo $val->author ?>
                                </div>
                                <div class="commentPublDate">
                                    <?php echo (date('j F Y H:i', $val->publicationDate)) ?>
                                </div>
                                <div class="commentText">
                                    <?php echo $val->comment ?>
                                </div>
                                <div class="separator"> </div>
                                <!-- Проверка разрешений -->
                                <?php if ($val->deleted===NULL) { ?>
                                    <?php if ($this->test['editCom']===true and 
                                        $_SESSION['user']['name']===$commentAuthor=$val->getAuthorOfComment()
                                        or $this->test['editAnyCom']===true) { ?>
                                        <div id="editComment" class="commentEditBut">
                                            <a class="commentEditBut_link" href="./editComment.php?id=<?php echo $val->id.'&amp;articleID='.$this->test['article']->id.'&amp;page='.intval($_GET['page']); ?>">Редактировать</a>
                                        </div>
                                    <?php } ?>
                                    <!-- Проверка разрешений -->
                                    <?php if ($this->test['deleteCom']===true and 
                                        $_SESSION['user']['name']===$commentAuthor=$val->getAuthorOfComment()
                                        or $this->test['deleteAnyCom']===true) { ?>
                                        <div id="deleteComment" class="commentDelBut">
                                            <a class="commentDelBut_link" href="./deleteComment.php?id=<?php echo $val->id.'&amp;articleID='.$this->test['article']->id.'&amp;page='.intval($_GET['page']); ?>">Удалить</a>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?> <!-- end foreach -->
                    </div> <!-- end Comments -->
                    <!-- Нумерация страниц -->
                    <?php if ($this->test['htmlNav']['htmlLastPage']!=0) { ?>
                        <div class="pageNav">
                            <ul id="navPages">
                                <!-- Формируем ссылки "в начало" и "предыдущая" -->
                                <?php $numberPage=$this->test['htmlNav']['htmlCurrentPage'];
                                if ($numberPage==1) { ?>
                                    <li>
                                        <span>В начало</span>
                                    </li>
                                    <li>
                                        <span>Предыдущая</span>
                                    </li>
                                <?php } else { ?>
                                    <li>
                                        <a href="<?php echo $this->test['htmlNav']['htmlNavStart'].'&id='.$this->test['article']->id; ?>">В начало</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $this->test['htmlNav']['htmlNavPrev'].'&id='.$this->test['article']->id; ?>">Предыдущая</a>
                                    </li>
                                <?php } ?>

                                <!-- Формируем ссылки c номерами страниц -->    
                                <?php foreach ($this->test['htmlNav']['htmlOut'] as $key => $val) { 
                                    if ($numberPage==$key) { ?>
                                        <li>
                                            <span><?php echo $key;?></span>
                                        </li>
                                    <?php } else { ?>
                                        <li>
                                            <a href="<?php echo $val.'&id='.$this->test['article']->id;?> "><?php echo $key;?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>

                                <!-- Формируем ссылки "следующая" и "в конец" -->
                                <?php if ($numberPage==$this->test['htmlNav']['htmlLastPage']) { ?>
                                    <li>
                                        <span>Следующая</span>
                                    </li>
                                    <li>
                                        <span>В конец</span>
                                    </li>
                                <?php } else { ?>
                                    <li>
                                        <a href="<?php echo $this->test['htmlNav']['htmlNavNext'].'&id='.$this->test['article']->id;?>">Следующая</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $this->test['htmlNav']['htmlNavEnd'].'&id='.$this->test['article']->id;?>">В конец</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div> <!-- pageNav -->
                    <?php } ?> <!-- Нумерация страниц -->
                </div> <!-- Конец список статей -->
            </div>
        </div>  
        <div class="sidebarRight">
            <div id="calendar" class="block">
                <?php echo $this->test['cal']; ?>
            </div>
            <div class="block user">
                <div class="title">
                    Hello, <?php  echo ($_SESSION['user']['name']); ?>!<br />
                </div>
                <div class="userList">
                    Your ID is <?php  echo ($_SESSION['user']['id']); ?>.<br />
                    Your E-mail is <?php  echo ($_SESSION['user']['email']); ?>.<br />
                    Your IP is <?php  echo ($_SERVER['REMOTE_ADDR']); ?>.<br />
                    <form method="post" action="assets/inc/process.inc.php">
                        <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                        <input type="hidden" name="action" value="user_logout" />
                        <input type="submit" value="Выход"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="footerPanel">
            Copyright © HelloWorldSite
        </div>
    </div>    
</div> <!--end #content-->