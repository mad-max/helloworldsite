<div class="sidebarRight">
    <div id="calendar" class="block">
        <?php echo $this->test['cal']; ?>
    </div>
    <div class="block user">
        <div class="title">
            Hello, <?php  echo ($_SESSION['user']['name']); ?>!<br />
        </div>
        <div class="userList">
            Your ID is <?php  echo ($_SESSION['user']['id']); ?>.<br />
            Your E-mail is <?php  echo ($_SESSION['user']['email']); ?>.<br />
            Your IP is <?php  echo ($_SERVER['REMOTE_ADDR']); ?>.<br />
            <form method="post" action="assets/inc/process.inc.php">
                <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                <input type="hidden" name="action" value="user_logout" />
                <input type="submit" value="Выход"/>
            </form>
        </div>
    </div>
</div>