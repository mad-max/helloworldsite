<div id="layout">
    <div id="navbar">
        <div class="navPanel">
            navPanel
        </div>
    </div>
    <div class="inner">
        <div class="content">
            <div class="siteName">
                <h1><a class="siteName" href="./main.php"><?php echo siteName; ?></a></h1>
            </div>
             <!-- menu -->
            <?php include "assets/templates/menu.tpl"; ?>
            <div class="postList">
                <div class="posts">
                    
                    <!-- Cтатья -->
                    
                    <div id="post_<?php echo $this->test['comment']->id; ?>" class="post">
                        
                        <div class="published">
                            <?php echo (date('j F Y H:i', $this->test['comment']->publicationDate)); ?>
                        </div>
                        <div class="content_html">
                            <?php echo $this->test['comment']->comment; ?>
                        </div>
                        <div class="published">
                            Дата редактирования: <?php if (!empty($this->test['comment']->editDate)) { echo (date('j F Y H:i', $this->test['comment']->editDate));} ?>
                        </div>
                        <div class="published">
                            Автор: <?php echo $this->test['comment']->author; ?>
                        </div>
                    </div>


                    <!-- Редактирование статьи -->
                    <form action="assets/inc/process.inc.php" method="post">
                        <fieldset>
                            <legend>Редактирование комментария</legend>

                            
                            <label for="article_description">Статья</label>
                            <div id="bbButtons">
                                <input id="quote-button" class="bbButton" type="button"  value="Цитата"  />
                                <input id="code-button" class="bbButton" type="button"  value="Код" />
                                <input id="bold-button" class="bbButton" type="button"  value="Ж" />
                                <input id="italic-button" class="bbButton" type="button"  value="К" />
                                <input id="underline-button" class="bbButton" type="button"  value="Ч" />
                                <input id="strike-button" class="bbButton" type="button"  value="П" />
                                <input id="url-button" class="bbButton" type="button"  value="Ссылка" />
                                <input id="img-button" class="bbButton" type="button"  value="Рисунок" />
                                <input id="video-button" class="bbButton" type="button"  value="Видео" />
                                <input id="size-button" class="bbButton" type="button"  value="Размер" />
                                <input id="color-button" class="bbButton" type="button"  value="Цвет" />
                                <input id="list-button" class="bbButton" type="button"  value="" />
                                <input id="listn-button" class="bbButton" type="button"  value="" />
                                <input id="listm-button" class="bbButton" type="button"  value="Пункт списка" />
                            </div>
                            <textarea name="content"
                                id="article_description"><?php echo $this->test['comment']->comment; ?></textarea>
                            
                            
                            <br />
                            <br />
                            <input type="hidden" name="commentID" value="<?php echo $this->test['comment']->id; ?>" />
                            <input type="hidden" name="page" value="<?php echo $this->test['currentPage']; ?>" />
                            <input type="hidden" name="id" value="<?php echo $this->test['article']->id; ?>" />
                            <input type="hidden" name="token" value="<?php echo ($_SESSION['token']); ?>" />
                            <input type="hidden" name="action" value="comment_edit" />
                            <input type="submit" name="article_submit" value="Отправить" />
                                 <a class="abort" href="./article.php?id=<?php echo $this->test['article']->id .'&amp;page='.$this->test['currentPage']?>">Отменить</a>
                        </fieldset>
                    </form>
                </div> <!-- Редактирование статей -->
            </div> <!-- postList -->
        </div>  <!-- content -->
        <!-- sidebarRight -->
        <?php include "assets/templates/sidebarRight.tpl"; ?> 
        <div class="clear"></div>
        <div class="footerPanel">
            Copyright © HelloWorldSite
        </div>
    </div>    
</div> <!--end #content-->
<?php //include "assets/templates/editComment.tpl"; ?>