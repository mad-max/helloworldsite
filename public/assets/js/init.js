/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Удостовериться в готовности документа, прежде чем выполнять сценарии
jQuery(function($){  
    var textarea = document.getElementById("article_description");
    $(".bbButton").click(function() {
        switch ($(this).attr("id")) {
            case 'quote-button':
                var val1='[quote]';
                var val2='[/quote]';
                break;
            case 'code-button':
                var val1='[code]';
                var val2='[/code]';
                break;
            case 'bold-button':
                var val1='[b]';
                var val2='[/b]';
                break;
            case 'italic-button':
                var val1='[i]';
                var val2='[/i]';
                break;
            case 'underline-button':
                var val1='[u]';
                var val2='[/u]';
                break;  
            case 'strike-button':
                var val1='[s]';
                var val2='[/s]';
                break;  
            case 'url-button':
                var val1='[url]';
                var val2='[/url]';
                break; 
            case 'img-button':
                var val1='[img]';
                var val2='[/img]';
                break; 
            case 'size-button':
                var val1='[size=100]';
                var val2='[/size]';
                break;     
            case 'color-button':
                var val1='[color=black]';
                var val2='[/color]';
                break;  
            case 'list-button':
                var val1='[list]';
                var val2='[/list]';
                break;
            case 'listn-button':
                var val1='[listn]';
                var val2='[/listn]';
                break;   
            case 'listm-button':
                var val1='[*]';
                var val2='[/*]';
                break; 
            case 'video-button':
                var val1='[video]';
                var val2='[/video]';
                break;
            default:
                var val1=0;
                var val2=0;
        }
  
        
        var len = textarea.value.length,
        start = textarea.selectionStart,
        end = textarea.selectionEnd,
        sel = textarea.value.substring(start, end),
        replace = val1 + sel + val2;
        textarea.value = textarea.value.substring(0,start) + replace + textarea.value.substring(end,len);

    });
    

//    function addBBcode(){
//        document.getElementById("article_description").innerHTML="Текст, текст, текст!";
//    }
    
//    var textarea = document.getElementById("article_description");
//    var button = document.getElementById("quote-button");
//    button.onclick = function() {
//        var id = $(this).attr("data-id");
//        alert (id);
//        var len = textarea.value.length,
//        start = textarea.selectionStart,
//        end = textarea.selectionEnd,
//        sel = textarea.value.substring(start, end),
//        replace = '<b>' + sel + '<b>';
//    textarea.value = textarea.value.substring(0,start) + replace + textarea.value.substring(end,len);
//    };
    
});