<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Включить необходимые файлы
 */
include_once '../sys/core/init.inc.php';    //Данный файл init.inc.php генерирует маркер защиты от CSRF 'token', загружает конфигурационную информацию из файла конфигурации, подключается к базе данных.

/*
* Перенапрвить незарегистрированного пользователя на
* основную страницу
*/
if (!isset($_SESSION['user'])) {
    header("Location: ./index.php");
    exit;
}

/*
 * Вывести начальную часть страницы
 */
$page_title="&laquo;HelloWorld!&raquo; site";
$css_files=array('main.css', 'normalize.css', 'global.css', 'edit.css', 'calstyle.css');
include_once 'assets/common/header.inc.php';


//filter_input — Принимает переменную извне PHP и, при необходимости, фильтрует ее
$id=filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$articleID=filter_input(INPUT_GET, 'articleID', FILTER_SANITIZE_NUMBER_INT);
$page=filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);


/*
 * Получаем из базы статью с заданным ID и комментарий.
 */
$article=Article::getById($articleID);
$comment=Comment::getById($dbo, $id);



/*
 * Заменяем bbcode на html теги.
 * Сохраняем в отдельном элементе массива, т. к. нам нужен и исходный вид текста.
 */
$comment->comment=$comment->get_cut_text($comment->comment, $C['commentLength']);
$comment->comment=$comment->replaceBBCode($comment->comment);




/*
 * Проверим ID в объекте статья. Если его нет значит статья удалена.
 */
if (empty ($comment->id)) {

    die ("ID is null");
}


/*
 * Загрузить календарь
 */
$date=date ('Y-m-d H:i:s');
$cal=new Calendar($dbo, $date);

$calendar=$cal->buildCalendar();
   


//Создаем ассоциативный массив из массива с объектом "статья"
$test=array ("article"=>$article, "comment"=>$comment, "cal"=>$calendar, "currentPage"=>$page);


//Создаем объект "шаблона"
$template = new Template("assets/templates/");  //путь к папке с шаблонами. должен заканчиваться /

//Устанавливаем свойтсва объекта
$template->set("test", $test);  //устанавливаем тестовую строку

//Выводим шаблон
$template->display("editComment");     //имя шаблона













/*
 * Вывести завершающую часть страницы
 */
include_once 'assets/common/footer.inc.php';
