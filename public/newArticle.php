<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Включить необходимые файлы
 */
include_once '../sys/core/init.inc.php';    //Данный файл init.inc.php генерирует маркер защиты от CSRF 'token', загружает конфигурационную информацию из файла конфигурации, подключается к базе данных.
    
/*
* Перенапрвить незарегистрированного пользователя на
* основную страницу
*/
if (!isset($_SESSION['user'])) {
    header("Location: ./index.php");
    exit;
}
    
/*
 * Вывести начальную часть страницы
 */
$page_title="&laquo;HelloWorld!&raquo; site";
$css_files=array('main.css', 'normalize.css', 'global.css', 'edit.css', 'calstyle.css');
include_once 'assets/common/header.inc.php';


/*
 * Получить список категорий.
 */
$listCategory=ext_db::getCategoryList();


/*
 * Получить список категорий.
 */
//$dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
//try {
//    $conn=new PDO($dsn, DB_USER, DB_PASS);
//} catch (Exception $e) {
//    //Если не удается установить соединение с БД, вывести сообщение об ошибке
//    die ($e->getMessage() );
//}
//$sql = "SELECT * FROM `category`";
//try {
//    $stmt=$conn->prepare($sql);
//    $stmt->execute();
//    $listCategory=array();
//    while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
//        $listCategory[$row['id_category']]=$row['name_category'];
//    }
//    $stmt->closeCursor();
//} catch (Exception $ex) {
//    die($ex->getMessage());
//}

//print_r ($listCategory);


/*
 * Загрузить календарь
 */
$date=date ('Y-m-d H:i:s');
$cal=new Calendar($dbo, $date);

$calendar=$cal->buildCalendar();
 

//Создаем ассоциативный массив из массива с объектом "статья"
$newArticleArray=array ('listCategory'=>$listCategory, "cal"=>$calendar);
    
//Создаем объект "шаблона"
$template = new Template("assets/templates/");  //путь к папке с шаблонами. должен заканчиваться /
    
//Устанавливаем свойтсва объекта
$template->set('newArticleArray', $newArticleArray);  //устанавливаем тестовую строку
    
//Выводим шаблон
$template->display("newArticle");     //имя шаблона













/*
 * Вывести завершающую часть страницы
 */
include_once 'assets/common/footer.inc.php';
