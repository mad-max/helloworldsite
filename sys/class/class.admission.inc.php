<?php



/**
 * Управляет выполнением административных задач.
 * 
 * Версия PHP 5
 * 
 * @author Max Maximov
 * @copyright (c) 2014 Max 
 */
class Admission extends DB_Connect {
    /**
     * Определяет длину затравки, используемой при хешировании паролей
     * 
     * @var int длина используемой затравки
     */
    private $_saltLength=7;
    
    /**
     * Сохраняет или создает объект БД и устанавливает длину затравки
     * 
     * @param object $db объект базы данных
     * @param int $$saltLength длина затравки для хеширования пароля
     */
    public function __construct($db = NULL, $saltLength=NULL) {
        parent::__construct($db);
        
        /*
         * Если передан целочисленный параметр, задать длину затравки
         */
        if (is_int($saltLength)) {  //определяет, является ли переменная integer.
            $this->_saltLength=$saltLength;
        }
    }   //__construct
    
    /**
     * Проверяет действительность учетных данных пользователя
     * Сохраняет данные пользователя в сессии
     * 
     * @return mixed TRUE в случае успешного завершения, иначе сообщение об ошибке
     */
    public function processLoginForm() {
        /*
         * Аварийное завершение, если был отправлен недействительный
         * атрибут ACTION
         */
        if ($_POST['action']!='user_login') {
            return "В processLoginForm передано недействительное значение атрибута ACTION";
        }
        
        /*
         * Маскировать пользовательский ввод в целях безопасности
         */
        $uname=htmlentities($_POST['userLogin'], ENT_QUOTES);   //Преобразует символы в соответствующие HTML сущности. 
        $pword=  htmlentities($_POST['userPassword'], ENT_QUOTES); //ENT_QUOTES Преобразуются и двойные, и одиночные кавычки. 
        
        /*
         * Извлечь из базы данных совпадающую информацию,
         * если она существует
         */
        $sql="SELECT `user_id`, `user_name`, `user_email`, `user_pass` "
                . "FROM `users` "
                . "WHERE `user_name`=:uname LIMIT 1";
        try {
            $stmt=$this->db->prepare($sql);
            $stmt->bindParam(':uname', $uname, PDO::PARAM_STR);
            $stmt->execute();
            $user=array_shift($stmt->fetchAll());   //array_shift-Извлечь первый элемент массива. fetchAll-Returns an array containing all of the result set rows 
            $stmt->closeCursor();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        /*
         * Аварийное завершение, если имя пользователя не согласуется ни с одной
         * записью в БД
         */
        if (!isset($user)) {
            return "Неверное имя пользователя или пароль.";
        }
        
        /*
         * Получить хеш-код пароля, предоставленного пользователем
         */
        $hash=$this->_getSaltedHash($pword, $user['user_pass']);
        
        /*
         * Проверить, совпадает ли хешированный пароль с сохраненным в БД хеш-кодом
         */
        if ($user['user_pass']==$hash) {
            /*
             * Сохранить пользовательскую информацию в сеансе
             * в виде массива
             */
            $_SESSION['user']=array(
                'id'=>$user['user_id'],
                'name'=>$user['user_name'],
                'email'=>$user['user_email']
            );
            return TRUE;
        } else {
            /*
             * Аварийное завершение в случае несовпадения паролей
             */
            return "Неверное имя пользователя или пароль";
        }
    }   //processLoginForm
    
    /**
     * Завершает сеанс пользователя
     * 
     * @return mixed TRUE в случае успешного завершения, иначе
     * сообщение об ошибке
     */
    public function processLogout() {
        /*
         * Аварийное завершение, если было передано неправильное значение
         * атрибута ACTION
         */
        if ($_POST['action']!='user_logout') {
            return "В processLogout было передано неправильное значение атрибута Action.";
        }
        
        /*
         * Удалить массив user из текущего сеанса
         */
        session_destroy();
        return TRUE;
    }

        /**
     * Генерирует хеш-код с затравкой для предоставленной строки
     * 
     * @param string $string подлежащая хешированию строка
     * @param string $salt отсюда извлечь затравку
     * @return string хеш-код с затравкой
     * 
     */
    private function _getSaltedHash($string, $salt=NULL) {
        /*
         * Сгенерировать затравку, если она не была предоставлена
         */
        if ($salt==NULL) {
            $salt=substr(md5(time()), 0, $this->_saltLength);   //substr() возвращает подстроку строки string длиной length, начинающегося с start символа по счету. 
            //md5 -- Возвращает MD5 хэш строки. time-Возвращает количество секунд, прошедших с начала Эпохи Unix
        } else {
            /*
             * Извлечь затравку из строки, если она была передана
             */
            $salt=substr($salt, 0, $this->_saltLength);
        }
        
        /*
         * Добавить затравку в хеш-код и возвратить его
         */
        return $salt.sha1($salt.$string);
    }   //_getSaltedHash
    
    
       public function testSaltedHash($string, $salt=NULL) {
           return $this->_getSaltedHash($string, $salt);
       }    //testSaltedHash
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
