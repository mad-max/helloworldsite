<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Класс для обработки статей
 *
 * @author Max
 */
class Article extends DB_Connect {
    //Свойства
    public $id=null;
    
    public $publicationDate=null;
    
    public $title=null;
    
    public $summary=null;
    
    public $content=null;
    
    public $img_src=null;
    
    public $category=null;
    
    public $editDate=null;
    
    public $author=null;
    
    
    public function __construct($db = NULL, $data=array() ) {
        parent::__construct($db);
        
        
        //Проверяем установлена ли переменная. Свойства id приводится к типу int с помощью (int)
        if (isset($data['id'])) { 
            $this->id=(int) $data['id']; 
        }
        
        //Проверяем установлена ли переменная. Свойства publicationDate приводится к типу int с помощью (int)
        if (isset($data['publicationDate'])) { 
            $this->publicationDate=(int) $data['publicationDate']; 
        }
        
        //Проверяем установлена ли переменная. Свойства title фильтруется с помощью регулярных выражений.
        if (isset($data['title'])) { 
            $this->title=$data['title'];
        }
        
        //Проверяем установлена ли переменная. Свойства summary фильтруется с помощью регулярных выражений.
        if (isset($data['summary'])) { 
            $this->summary=$data['summary'];
        }
                
       //Проверяем установлена ли переменная. 
        if (isset($data['content'])) { 
            $this->content=$data['content'];
        }
        
        //Проверяем установлена ли переменная. 
        if (isset($data['img_src'])) { 
            $this->img_src=$data['img_src'];
        }
        
        //Проверяем установлена ли переменная. Свойства category приводится к типу int с помощью (int)
        if (isset($data['category'])) { 
            $this->category=(int) $data['category'];
        }
        
        //Проверяем установлена ли переменная. 
        if (isset($data['editDate'])) { 
            $this->editDate=(int) $data['editDate'];
        }
        
        //Проверяем установлена ли переменная. 
        if (isset($data['author'])) { 
            $this->author= $data['author'];
        }
        
    }   //__construct
    
    
    /**
     * Предназначен для проверки формы и сохранения или
     * редактирования события. 
     * Проверяет права пользователя на добавление и редактирование статьи.
     * 
     * Перенести присваивание свойств в конструктор???
     * 
     * @return mixed TRUE в случае успешного завершения или сообщение об ошибке в случае сбоя
     */
    public function saveArticle () {
        /*
         * Выход, если значение "action" задано неправильно
         */
        if ($_POST['action']!='article_edit' and $_POST['action']!='article_new') {
            return "Некорректная попытка вызова метода saveArticle";
        }
        

        /*
         * Извлечь данные из формы
         */
        $this->title=filter_input(INPUT_POST, 'title', FILTER_SANITIZE_SPECIAL_CHARS);
        if (strlen($this->title)==0) {
            die("title is null.");
        }
        
//        if (strlen($_POST['title'])!=0) {
//            //Преобразует символы в соответствующие HTML сущности.
//            $this->title=htmlentities($_POST['title'], ENT_QUOTES, "UTF-8");
//        } else {
//            die("title is null.");
//        }
               
        //$this->summary=htmlentities($_POST['summary'], ENT_QUOTES, "UTF-8");
        $this->summary=filter_input(INPUT_POST, 'summary', FILTER_SANITIZE_SPECIAL_CHARS);
        
        //$this->content=htmlentities($_POST['content'], ENT_QUOTES, "UTF-8");
        $this->content=filter_input(INPUT_POST, 'content', FILTER_SANITIZE_SPECIAL_CHARS);
        
        
        $this->img_src=filter_input(INPUT_POST, 'img_src', FILTER_SANITIZE_SPECIAL_CHARS);
        if (strlen($this->img_src)==0) {
            $this->img_src=NULL;
        }
//        if (strlen($_POST['img_src'])!=0) {
//            $this->img_src=htmlentities($_POST['img_src'], ENT_QUOTES, "UTF-8");
//            //$this->img_src=filter_input(INPUT_POST, 'img_src', FILTER_SANITIZE_ENCODED);
//        } else {
//            $this->img_src=NULL;
//        }
        
        
        //$this->category=htmlentities($_POST['category'], ENT_QUOTES, "UTF-8");
        $this->category=filter_input(INPUT_POST, 'category', FILTER_SANITIZE_SPECIAL_CHARS);

        $this->id=filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        
        
        /*
         * Если ID не был передан, создать новую статью.
         */
        if (empty($this->id)) {
            
            //$this->author=htmlentities($_POST['author'], ENT_QUOTES, "UTF-8");
            $this->author=$_SESSION['user']['name'];
        
            //Устанавливаем дату создания новой статьи
            $this->publicationDate=time();
            
            //Проверяем разрешения пользователя. Добавление статьи.
            if (!Role::hasPrivilege($_SESSION['user']['id'], 'add article')) {
                return "Недостаточно прав для добавления статьи";
            }
        
            //Вставляем статью
            //Используем функцию MySQL FROM_UNIXTIME() для конвертации даты публикации в формат MySQL
            $sql = "INSERT INTO articles (publicationDate, title, summary, "
                . "content, img_src, category,  author)"
                . " VALUES (FROM_UNIXTIME(:publicationDate), :title, :summary, "
                . ":content, :img_src, :category,  :author)";
        
            try {
                $stmt=$this->db->prepare($sql);
                $stmt->bindParam(":publicationDate", $this->publicationDate, PDO::PARAM_INT); 
                $stmt->bindParam(":title", $this->title, PDO::PARAM_STR); 
                $stmt->bindParam(":summary", $this->summary, PDO::PARAM_STR);
                $stmt->bindParam(":content", $this->content, PDO::PARAM_STR);
                $stmt->bindParam(":img_src", $this->img_src, PDO::PARAM_STR);
                $stmt->bindParam(":category", $this->category, PDO::PARAM_INT);
                $stmt->bindParam(":author", $this->author, PDO::PARAM_STR);
                $stmt->execute();
                $stmt->closeCursor();   
                
            } catch (Exception $e) {
                die ($e->getMessage());
            } 
            
        } else {    //если id был передан - редактируем существующую статью
            $editPerm=Role::hasPrivilege($_SESSION['user']['id'], 'edit any article');
            if ($editPerm===false) {
                $editPerm=Role::hasPrivilege($_SESSION['user']['id'], 'edit own article');
                if ($editPerm===true) {
                    /*
                     *Узнаем по id кто автор для проверки разрешения на редактирования статьи.
                     */
                    $sql = "SELECT `author` FROM `articles` WHERE `id` = :id LIMIT 1";
                    try {
                        $stmt=$this->db->prepare($sql);
                        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
                        $stmt->execute();
                        $author=$stmt->fetch(PDO::FETCH_ASSOC);
                        $stmt->closeCursor();
                    } catch (Exception $ex) {
                        die($ex->getMessage());
                    }
            
                    /*
                     * Проверяем, что пользователь редактирует свою статью, а не чужую.
                     */
                    if ($author['author']!==$_SESSION['user']['name']) {
                        return "Недостаточно прав для редактирования статьи";
                    }
                } else {
                    return "Недостаточно прав для редактирования статьи";
                }
            }
            
            /*
             * Устанавливаем дату редактирования существующей статьи.
             */
            $this->editDate=time();
            
            /*
             * Обновить событие.
             */
            $sql = "UPDATE articles SET editDate=FROM_UNIXTIME(:editDate),"
                . " title=:title, summary=:summary, content=:content , img_src=:img_src,"
                . " category=:category WHERE id = :id";
            
            try {
                $stmt=$this->db->prepare($sql); 
                $stmt->bindParam(":title", $this->title, PDO::PARAM_STR); 
                $stmt->bindParam(":summary", $this->summary, PDO::PARAM_STR);
                $stmt->bindParam(":content", $this->content, PDO::PARAM_STR);
                $stmt->bindParam(":img_src", $this->img_src, PDO::PARAM_STR);
                $stmt->bindParam(":category", $this->category, PDO::PARAM_INT);
                $stmt->bindParam(":id", $this->id, PDO::PARAM_INT);
                $stmt->bindParam(":editDate", $this->editDate, PDO::PARAM_INT);
                $stmt->execute();
                $stmt->closeCursor();   
            } catch (Exception $e) {
                die ($e->getMessage());
            } 
            
        }   //end if
        
        /*
         * После выполнения запроса, метод возвращает ID новой статьи с помощью 
         * функции PDO lastInsertId() и сохраняет значение в свойстве $id
         */
        $this->id=$this->db->lastInsertId();
        
        return true;
    }   //saveArticle
    
    
//    //Не используется пока. Удалить?
//    public function storeFormValues ($params) {
//        //Сохраняем все параметры
//        $this->__construct($params);
//           
//        //Создаем переменную $publicationDate
//        //Разбираем и сохраняем в ней дату публикации
//        if (isset($params['publicationDate'])) {
//                
//            //explode -- Разбивает строку на подстроки 
//            $publicationDate=explode('-', $params['publicationDate']);
//           
//            //count -- Посчитать количество элементов массива или количество свойств объекта
//            //создать переменные $year, $month, $day
//            //list --  Присвоить переменным из списка значения подобно массиву 
//            //mktime -- Возвращает метку времени для заданной даты
//            //int mktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
//            if (count($publicationDate)===3) {
//                list($year, $month, $day)=$publicationDate;
//                $this->publicationDate=mktime(0, 0, 0, $month, $day, $year);
//            }
//        }
//    }    //storeFormValues
    
    /**
     * Статический метод
     * Возвращает объект статьи соответствующий заданному ID
     * 
     * @param int $id ID статьи
     * 
     * @return mixed Объект статьи или false
     */
    public static function getById ($id) {
        //Здесь выполняется соединение с базой данных MySQL
        //Данный дескриптор используется в остальном коде для обмена данных с базой.
//        $dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
//        try {
//            $conn=new PDO($dsn, DB_USER, DB_PASS);
//        } catch (Exception $e) {
//            //Если не удается установить соединение с БД, вывести сообщение об ошибке
//            die ($e->getMessage() );
//        }
        
        $conn=Article::staticDbConnect();
        
        //Выражение SELECT возвращает все поля (*) из записи в таблице articles, которые соответствуют заданному полю id.
        //Значение поля publicationDate возвращается в формате времени UNIX, вместо формата для дат MySQL, что упрощает процесс сохранения в нашем объекте.
        //Вместо того, чтобы помещать наш параметр $id непосредственно в строку SELECT, что увеличивает риск нарушения системы безопасности, мы используем :id.
        //Такой параметр известен как placeholder (указатель места размещения).
        //Далее мы вызываем метод PDO для привязывания значение $id к указателю места размещения.
        $sql = "SELECT *, UNIX_TIMESTAMP(publicationDate) AS `publicationDate`, UNIX_TIMESTAMP(editDate) AS `editDate` FROM `articles` WHERE `id` = :id LIMIT 1";
        
        try {
            //Сразу после сохранения выражения SELECT в строке, мы подготавливаем его с помощью функции $conn->prepare(), сохраняя полученный дескриптор в переменной  $st.
            //Подготовка выражения используется для работы со многими базами данных. Она позволяет выполнять запросы быстрее и безопаснее.
            $stmt=$conn->prepare($sql);
            
            //Затем мы привязываем значение переменной $id ( ID нужной статьи) к указателю места размещения :id с помощью вызова метода bindValue().
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            
            //И вызываем метод execute() для выполнения запроса.
            $stmt->execute();
            
            //После чего используем метод fetch() для перемещения полученной 
            //записи в ассоциированный массив с именами полей и соответствующими значениями, 
            //который хранится в переменной $article.
            $article=$stmt->fetch(PDO::FETCH_ASSOC);    
            
            //closeCursor() освобождает соединение с сервером, давая возможность
            // запускать другие SQL запросы. Метод оставляет запрос в состоянии готовности к повторному запуску.
            $stmt->closeCursor();
            
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        if ($article) {
            return new Article ("", $article);
            //$bbToHtml=$val->replaceBBCode($val->content);
            //$val->content=$bbToHtml;
            //return $val;
        }
        
    }   //getById
    
    
    /**
     * Возвращает все или диапазон обеъектов статей в базе данных
     * 
     * @param int $numRows Кол-во строк (по умолчанию все)
     * @param string $order Столбец по которому производится сортировка статей (по умолчанию publicationDate DESC)
     * @return mixed Двухэлементный массив: results=>массив, список объектов статей; totalRows=>общее кол-во статей
     * 
     */
    public static function getList ($start=0, $numRows=1000, $order="publicationDate DESC") {
        
        $conn=Article::staticDbConnect();
        
        
        
        //Выражение SELECT возвращает ??? в таблице articles, 
        //которые соответствуют ???.
        //Значение поля publicationDate возвращается в формате времени UNIX, вместо формата для дат MySQL, 
        //что упрощает процесс сохранения в нашем объекте.
        //Вместо того, чтобы помещать наш параметр $numRows непосредственно в строку SELECT,
        // что увеличивает риск нарушения системы безопасности, мы используем :numRows.
        //Такой параметр известен как placeholder (указатель места размещения).
        //Далее мы вызываем метод PDO для привязывания значение $numRows к указателю места размещения.
        //mysql_escape_string() - отбросить любые специальные символы (для безопасности)
        $sql = "SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP (publicationDate) "
                . "AS publicationDate FROM articles ORDER BY "
                . mysql_escape_string($order)." LIMIT :startRow, :numRows";
        
        try {
            //Сразу после сохранения выражения SELECT в строке, мы подготавливаем его
            // с помощью функции $conn->prepare(), сохраняя полученный дескриптор в переменной  $st.
            //Подготовка выражения используется для работы со многими базами данных. 
            //Она позволяет выполнять запросы быстрее и безопаснее.
            $stmt=$conn->prepare($sql);
            
            //Затем мы привязываем значение переменной $numRows к указателю места 
            //размещения :numRows с помощью вызова метода bindValue().
            $stmt->bindParam(':startRow', $start, PDO::PARAM_INT);
            $stmt->bindParam(':numRows', $numRows, PDO::PARAM_INT);
            
            
            //И вызываем метод execute() для выполнения запроса.
            $stmt->execute();
            
            $list=array();

            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
                $article=new Article("", $row);
                $list[]=$article;
            }
            
            //closeCursor() освобождает соединение с сервером, давая возможность 
            //запускать другие SQL запросы. Метод оставляет запрос в состоянии готовности к повторному запуску.
            $stmt->closeCursor();
            
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        //Получаем общее кол-во статей, которые соответствуют критерию
        $sql = "SELECT FOUND_ROWS() AS totalRows";
	$totalRows = $conn->query( $sql )->fetch(PDO::FETCH_ASSOC);

        return (array ("results"=>$list, "totalRows"=>$totalRows));
        
    }   //getList
    
    
    public static function getListByCategory ($start=NULL, $numRows=1000, $category=NULL, $order="publicationDate DESC") {
        
        if (empty($category)) {
            die ("getListByCategory. category is NULL");
        }
        $conn=Article::staticDbConnect();
        
        $idCategory=ext_db::getIdCategoryByName ($category);
        
         if (empty($idCategory)) {
            die ("getListByCategory. category is not exists");
        }
        
        
        //Выражение SELECT возвращает ??? в таблице articles, 
        //которые соответствуют ???.
        //Значение поля publicationDate возвращается в формате времени UNIX, вместо формата для дат MySQL, 
        //что упрощает процесс сохранения в нашем объекте.
        //Вместо того, чтобы помещать наш параметр $numRows непосредственно в строку SELECT,
        // что увеличивает риск нарушения системы безопасности, мы используем :numRows.
        //Такой параметр известен как placeholder (указатель места размещения).
        //Далее мы вызываем метод PDO для привязывания значение $numRows к указателю места размещения.
        //mysql_escape_string() - отбросить любые специальные символы (для безопасности)
        $sql = "SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP (publicationDate) "
                . "AS publicationDate FROM articles WHERE category = :idCategory ORDER BY "
                . mysql_escape_string($order)." LIMIT :startRow, :numRows";
        
        try {
            //Сразу после сохранения выражения SELECT в строке, мы подготавливаем его
            // с помощью функции $conn->prepare(), сохраняя полученный дескриптор в переменной  $st.
            //Подготовка выражения используется для работы со многими базами данных. 
            //Она позволяет выполнять запросы быстрее и безопаснее.
            $stmt=$conn->prepare($sql);
            
            //Затем мы привязываем значение переменной $numRows к указателю места 
            //размещения :numRows с помощью вызова метода bindValue().
            $stmt->bindParam(':startRow', $start, PDO::PARAM_INT);
            $stmt->bindParam(':numRows', $numRows, PDO::PARAM_INT);
            $stmt->bindParam(':idCategory', $idCategory["id_category"], PDO::PARAM_INT);
            
            //И вызываем метод execute() для выполнения запроса.
            $stmt->execute();
            
            $list=array();

            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
                $article=new Article("", $row);
                $list[]=$article;
            }
            
            //closeCursor() освобождает соединение с сервером, давая возможность 
            //запускать другие SQL запросы. Метод оставляет запрос в состоянии готовности к повторному запуску.
            $stmt->closeCursor();
            
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        //Получаем общее кол-во статей, которые соответствуют критерию
        $sql = "SELECT FOUND_ROWS() AS totalRows";
	$totalRows = $conn->query( $sql )->fetch(PDO::FETCH_ASSOC);

        return (array ("results"=>$list, "totalRows"=>$totalRows));
        
    }   //getListByCategory
    
    
    
//    /**
//     * Вставляем текущий объект статьи в базу данных. Не используется пока. Удалить?
//     * 
//     */
//    public function insert() {
//        //Сначала проверяем, что объект не имеет установленного свойства $id. 
//        //Если у объекта есть ID, то, вероятно, статья уже имеется в базе данных и ее добавлять не нужно.
//        if (!is_null ($this->id)) {
//            trigger_error ("Article::insert(): Attempt to insert an Article "
//                    . "object that already has its ID property set "
//                    . "(to $this->id).", E_USER_ERROR);
//            
//        }
//        
//        //Вставляем статью
//        //Используем функцию MySQL FROM_UNIXTIME() для конвертации даты публикации в формат MySQL
//        $sql = "INSERT INTO articles ( publicationDate, title, summary, content, img_src, category )"
//                . " VALUES ( FROM_UNIXTIME(:publicationDate), :title, :summary, :content, :img_src, :category )";
//        
//        $stmt=$this->db->prepare($sql);
//        $stmt->bindParam(":publicationDate", $this->publicationDate, PDO::PARAM_INT); 
//        $stmt->bindParam(":title", $this->title, PDO::PARAM_STR); 
//        $stmt->bindParam(":summary", $this->summary, PDO::PARAM_STR);
//        $stmt->bindParam(":content", $this->content, PDO::PARAM_STR);
//        $stmt->bindParam(":img_src", $this->img_src, PDO::PARAM_STR);
//        $stmt->bindParam(":category", $this->category, PDO::PARAM_INT);
//        $stmt->execute();
//        
//        //После выполнения запроса, метод возвращает ID новой статьи с помощью 
//        //функции PDO lastInsertId() и сохраняет значение в свойстве $id
//        $this->id=$this->db->lastInsertId();
//        
//        $stmt->closeCursor();
//        
//    }   //insert
    
//    
//    /*
//     * Обновляем текущий объект статья в базе данных. Не используется пока. Удалить?
//     */
//    public function update() {
//        //Сначала проверяем, что объект имеет установленноое свойство $id. 
//        if (is_null ($this->id)) {
//            trigger_error ("Article::update(): Attempt to update an Article "
//                    . "object that that does not have its ID property set ",
//                      E_USER_ERROR);
//            
//        }
//        
//        //Обновляем статью
//        //Используем функцию MySQL FROM_UNIXTIME() для конвертации даты публикации в формат MySQL
//        $sql = "UPDATE articles SET publicationDate=FROM_UNIXTIME(:publicationDate),"
//                . " title=:title, summary=:summary, content=:content , img_src=:img_src,"
//                . " category=:category WHERE id = :id";
//        
//        $stmt=$this->db->prepare($sql);
//        $stmt->bindParam(":publicationDate", $this->publicationDate, PDO::PARAM_INT); 
//        $stmt->bindParam(":title", $this->title, PDO::PARAM_STR); 
//        $stmt->bindParam(":summary", $this->summary, PDO::PARAM_STR);
//        $stmt->bindParam(":content", $this->content, PDO::PARAM_STR);
//        $stmt->bindParam(":img_src", $this->img_src, PDO::PARAM_STR);
//        $stmt->bindParam(":category", $this->category, PDO::PARAM_INT);
//        $stmt->bindParam(":id", $this->id, PDO::PARAM_INT);
//        $stmt->execute();
//        $stmt->closeCursor();
//        
//    }   //update
    
    /**
     * Удаляем текущий объект статья из базы данных
     */
    public function delete () {
        
        /*
         * Если ID не был передан-ошибка
         */
        $this->id=filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
       
        //Сначала проверяем, что объект имеет установленноое свойство $id. 
        if (is_null ($this->id)) {
            trigger_error ("Article::delete(): Attempt to delete an Article "
                    . "object that that does not have its ID property set ",
                      E_USER_ERROR);
            
        }
        
        /*
         * Проверяем разрешения пользователя. Удаление статьи.
         */
        $deletePerm=Role::hasPrivilege($_SESSION['user']['id'], 'delete any article');
        if ($deletePerm===false)  {
            $deletePerm=Role::hasPrivilege($_SESSION['user']['id'], 'delete own article');
            if ($deletePerm===true) {
                /*
                 * Узнаем по id кто автор для проверки разрешения на удаление статьи.
                 */
                $sql = "SELECT `author` FROM `articles` WHERE `id` = :id LIMIT 1";
                try {
                    $stmt=$this->db->prepare($sql);
                    $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
                    $stmt->execute();
                    $author=$stmt->fetch(PDO::FETCH_ASSOC);
                    $stmt->closeCursor();
                } catch (Exception $ex) {
                    die($ex->getMessage());
                }
                /*
                 * Проверяем, что пользователь удаляет свою статью, а не чужую.
                 */
                if ($author['author']!==$_SESSION['user']['name']) {
                    return "Недостаточно прав для удаления статьи";
                }       
            } else {
                return "Недостаточно прав для удаления статьи";
            }
            
        }
        
        
        $sql = "DELETE FROM articles WHERE id =:id LIMIT 1";
        
        $stmt=$this->db->prepare($sql);
        $stmt->bindParam(":id", $this->id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
        
        return TRUE;
        
    }   //delete
    
    public function replaceBBCode($text_post) {
        $str_search = array(
        "#&\#13;&\#10;#is",
        "#\[b\](.+?)\[\/b\]#is",
        "#\[i\](.+?)\[\/i\]#is",
        "#\[u\](.+?)\[\/u\]#is",
        "#\[s\](.+?)\[\/s\]#is",
        "#\[code\](.+?)\[\/code\]#is",
        "#\[quote\](.+?)\[\/quote\]#is",
        "#\[url=(.+?)\](.+?)\[\/url\]#is",
        "#\[url\](.+?)\[\/url\]#is",
        "#\[img\](.+?)\[\/img\]#is",
        "#\[size=(.+?)\](.+?)\[\/size\]#is",
        "#\[color=(.+?)\](.+?)\[\/color\]#is",
        "#\[list\](.+?)\[\/list\]#is",
        "#\[listn](.+?)\[\/listn\]#is",
        "#\[\*\](.+?)\[\/\*\]#",
        "#\[video\](.+?)\[\/video\]#is"

        );
        $str_replace = array(
        "<br />",
        "<b>\\1</b>",
        "<i>\\1</i>",
        "<span style='text-decoration:underline'>\\1</span>",
        "<span style='text-decoration:line-through'>\\1</span>",
        "<code class='code'>\\1</code>",
        "<blockquote><span>Цитата:</span> <hr/>\\1<hr/></blockquote>",
        "<a href='\\1'>\\2</a>",
        "<a href='\\1'>\\1</a>",
        "<img align='left' src=\\1 alt='Изображение'/>",
        "<span style='font-size:\\1%'>\\2</span>",
        "<span style='color:\\1'>\\2</span>",
        "<ul>\\1</ul>",
        "<ol>\\1</ol>",
        "<li>\\1</li>",
        //"<video controls='controls' src=\\1></video>"
        '<iframe align="left" width="420" height="315" src=//\\1 frameborder="0" allowfullscreen></iframe>'

        );
    
        return preg_replace($str_search, $str_replace, $text_post);
        
    } //replaceBBCode
    
    /*
     * Обрезает строку до нужного кол-ва символов.
     * Убирает теги.
     */
    public function get_cut_text($text,$length) {
        if (strlen($text)>$length) {
            //Удаляет HTML и PHP тэги из строки
            $text = strip_tags($text);
            
            //возвращает подстроку строки string длиной length, начинающегося с start символа по счету.
            $substring_limited = substr($text,0,$length); 
            //Возвращает позицию последнего вхождения символа
            return substr($substring_limited, 0, strrpos($substring_limited, ' ' )).'...';
        } else {
            return strip_tags($text);
        }
    }
  
    private function staticDbConnect () {
        //Здесь выполняется соединение с базой данных MySQL
        //Данный дескриптор используется в остальном коде для обмена данных с базой.
        $dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
        try {
            $conn=new PDO($dsn, DB_USER, DB_PASS);
        } catch (Exception $e) {
            //Если не удается установить соединение с БД, вывести сообщение об ошибке
            die ($e->getMessage() );
        }
        return $conn;
    }
    
}   //class Article 


















