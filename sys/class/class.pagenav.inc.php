<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * СОздает нумерацию страниц
 * 
 * 
 *
 * @author Max
 */
class pageNav {
    //protected $id="pagination";
    //protected $startChar="&laquo";
    //protected $prevChar="&lsaquo";
    //protected $nextChar="&rsaquo";
    //protected $endChar="&raquo";
    private $varName="page";
    
    private $wholeArticles=0;
    //protected $needChunk=0;
    //protected $queryVars=array();
    //protected $pagesArr=array();
    private $link=NULL;
    private $limit=NULL;
    private $linkLimit=NULL;
    private $amountPages=NULL;
    private $currentPage=NULL;
    private $offsetFirstPage=NULL;
    private $offsetLastPage=NULL;
    private $pagesArr=array();
    private $prev=NULL;
    private $next=NULL;
    private $startChunk=NULL;
    private $endChunk=NULL;


    private $htmlOut=array();
    private $htmlNavStart='';
    private $htmlNavPrev='';
    private $htmlNavNext='';
    private $htmlNavEnd='';
    
    
    /**
     * 
     * @param int $allPages Кол-во статей в БД
     * @param int $limitArticles Кол-во выводимых статей
     * @param int $navLinkLimit Кол-во выводимых ссылок-номеров страниц
     */
    public function __construct($allPages=NULL, $limitArticles=NULL, $navLinkLimit=NULL) {
        $this->wholeArticles=$allPages;
        $this->limit=$limitArticles;
        $this->linkLimit=$navLinkLimit;
    }
    
    /**
     * 
     * @param int $currentOffset Смещение для БД
     * @return array
     */
    public function getNavLinks($currentOffset, $category=NULL) {
        
        //$_SERVER['PHP_SELF']-Имя файла скрипта, который сейчас выполняется,
        // относительно корня документов. 
        $this->link=$_SERVER['PHP_SELF']; 
        
        //ceil -- Округляет дробь в большую сторону
        $this->amountPages=ceil($this->wholeArticles / $this->limit);
        
        //Создаем массив, где ключ - это номер страницы,
        //а значение массива - смещения для БД
        for ($i=0; $i<$this->amountPages; $i++) {
            $this->pagesArr[$i+1] = $i * $this->limit;
        }
        
        //Ищем в массиве номер страницы по смещению
        $this->currentPage=array_search($currentOffset, $this->pagesArr);
        
        //В начало
        $this->offsetFirstPage=0;
        
        //В конец
        $this->offsetLastPage=$this->pagesArr[$this->amountPages];
                
        //Предыдущая ссылка
        if ($this->currentPage > 1) {
            $this->prev=$this->pagesArr[$this->currentPage - 1];
        } else {
            $this->prev=$this->offsetFirstPage;
        }
        
        //Следующая ссылка
        if ($this->currentPage < $this->amountPages) {
            $this->next=$this->pagesArr[$this->currentPage + 1];
        } else {
            $this->next=$this->pagesArr[$this->currentPage];
        }
        
        //Крайняя левая ссылка в выводимой части
        //floor -- Округляет дробь в меньшую сторону
        if ($this->prev<floor($this->linkLimit/2)) {
            $this->startChunk=1;
        } else {
            $this->startChunk=$this->currentPage - floor($this->linkLimit / 2);
        }
        
        //Крайняя правая ссылка в выводимой части
        $this->endChunk=$this->startChunk+$this->linkLimit - 1;
        
        if ($this->endChunk > $this->wholeArticles) {
            $this->startChunk -=($this->endChunk - $this->wholeArticles);
            $this->endChunk=  $this->wholeArticles;
            if ($this->startChunk < 1) { $this->startChunk=1; }
        }
                
        
        $this->htmlNavStart .=$this->link.'?'.$this->varName.'='.$this->offsetFirstPage
                .$cat=isset($category) ? ('&amp;category='.urlencode($category)) : NULL;
        
        $this->htmlNavPrev .=$this->link.'?'.$this->varName.'='.  $this->prev.
                $cat=isset($category) ? ('&amp;category='.urlencode($category)) : NULL;
        
         
        $this->htmlNavNext .= $this->link.'?'.$this->varName.'='.  
                $this->next.$cat=isset($category) ? ('&amp;category='.urlencode($category)) : NULL;
        
        $this->htmlNavEnd .= $this->link.'?'.$this->varName.'='.  
                $this->offsetLastPage.$cat=isset($category) ? ('&amp;category='.urlencode($category)) : NULL;
        
        
        for ($i = $this->startChunk; $i <= $this->endChunk; $i++) {
            $this->htmlOut[$i]=$this->link.'?'.$this->varName. '='.  
                    $this->pagesArr[$i].$cat=isset($category) ? 
                    ('&amp;category='.urlencode($category)) : NULL;
            if ($i>=$this->amountPages) {
                break;
            }
        }
        
        $navArray=array('htmlNavStart', 'htmlNavPrev', 'htmlOut', 'htmlNavNext', 'htmlNavEnd', 'htmlPagesNum');
        $navArray['htmlNavStart']=  $this->htmlNavStart;
        $navArray['htmlNavPrev']=  $this->htmlNavPrev;
        $navArray['htmlOut']=  $this->htmlOut;
        $navArray['htmlNavNext']=  $this->htmlNavNext;
        $navArray['htmlNavEnd']=  $this->htmlNavEnd;
        $navArray['htmlCurrentPage']=$this->currentPage;
        $navArray['htmlLastPage']=$this->amountPages;
        
        return $navArray;
        
    }
    















}
