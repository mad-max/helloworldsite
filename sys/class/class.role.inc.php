<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Проверяет есть ли требуемое разрешение у пользователя.
 *
 * @author Max
 */
class Role extends DB_Connect {
    
    /**
     *
     * @var array массив с разрешениями
     */
    protected $permissions;
    
    /**
     *
     * @var array массив с классами "роль"
     */
    public $roles;


    /**
     * Создает массив для разрешений
     */
    protected function __construct() {
        parent::__construct();
        $this->permissions = array();
        $this->roles = array();
    }
    
    /**
     * Заполняет массив соответствующими полномочиями
     * 
     * @param int $role_id Идентификатор роли
     */
    private function getRolePerms($role_id) {
        
                
        $sql="SELECT t2.perm_desc FROM role_perm AS t1"
                . " JOIN permissions AS t2 ON t1.perm_id=t2.perm_id"
                . " WHERE t1.role_id=:role_id";
        
        
        //Здесь выполняется соединение с базой данных MySQL
        //Данный дескриптор используется в остальном коде для обмена данных с базой.
        //$dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
        //try {
        //    $conn=new PDO($dsn, DB_USER, DB_PASS);
        //} catch (Exception $e) {
            //Если не удается установить соединение с БД, вывести сообщение об ошибке
        //    die ($e->getMessage() );
        //}
        
        try {
            //Сразу после сохранения выражения SELECT в строке, мы подготавливаем его с помощью функции $conn->prepare(), сохраняя полученный дескриптор в переменной  $st.
            //Подготовка выражения используется для работы со многими базами данных. Она позволяет выполнять запросы быстрее и безопаснее.
            $stmt=$this->db->prepare($sql);
            
            //Затем мы привязываем значение переменной $id ( ID нужной статьи) к указателю места размещения :id с помощью вызова метода bindValue().
            $stmt->bindParam(':role_id', $role_id, PDO::PARAM_INT);
            
            //И вызываем метод execute() для выполнения запроса.
            $stmt->execute();
            
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->permissions[$row["perm_desc"]] = true;
            }   
            
            
            //closeCursor() освобождает соединение с сервером, давая возможность
            // запускать другие SQL запросы. Метод оставляет запрос в состоянии готовности к повторному запуску.
            $stmt->closeCursor();
            
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        return true;
        
    }   //getRolePerms
    
    
    
    private  function initRoles($user_id) {
        
        
        //$userRoles=new Role();
        
        $sql = "SELECT t1.role_id, t2.role_name FROM user_role as t1"
                ." JOIN roles as t2 ON t1.role_id = t2.role_id"
                ." WHERE t1.user_id = :user_id";
        
        //Здесь выполняется соединение с базой данных MySQL
        //Данный дескриптор используется в остальном коде для обмена данных с базой.
        //$dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
        //try {
        //    $conn=new PDO($dsn, DB_USER, DB_PASS);
        //} catch (Exception $e) {
            //Если не удается установить соединение с БД, вывести сообщение об ошибке
        //   die ($e->getMessage() );
        //}
        
        try {
            //Сразу после сохранения выражения SELECT в строке, мы подготавливаем его с помощью функции $conn->prepare(), сохраняя полученный дескриптор в переменной  $st.
            //Подготовка выражения используется для работы со многими базами данных. Она позволяет выполнять запросы быстрее и безопаснее.
            $stmt=$this->db->prepare($sql);
            
            //Затем мы привязываем значение переменной $id ( ID нужной статьи) к указателю места размещения :id с помощью вызова метода bindValue().
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            
            //И вызываем метод execute() для выполнения запроса.
            $stmt->execute();
            
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->roles[$row["role_name"]] = $this->getRolePerms($row["role_id"]);
                
            }
            
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        return true;
        
    }   //initRoles
    
    
    // Проверка установленных полномочий
    //public function hasPerm($permission) {
    //    return isset($this->permissions[$permission]);
    //}
    
    
    // Проверяем, обладет ли пользователь нужными разрешениями
    public static function hasPrivilege($user_id, $perm) {
        
        $userRoles=new Role();
        
        $userRoles->initRoles($user_id);
        
        if (isset($userRoles->permissions[$perm])) {
            return true;
        } else {
            return false;
        }

    }   //hasPrivilege
    
    
    
    
}   //Role
