<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Класс для обработки комментариев
 *
 * @author Max
 */
class Comment extends DB_Connect {
    /**
     * id комментария
     * 
     * @var int уникальный идентификатор комментария
     */
    public $id=NULL;
    
    /**
     *
     * @var string автор комментария 
     */
    public $author=NULL;
    
    /**
     *Дата написания комментария
     * 
     * Формат хранения: ГГГГ-ММ-ДД ЧЧ:ММ:СС
     * 
     * @var int 
     */
    public $publicationDate=NULL;
    
    /**
     *Дата редактирования комментария
     * 
     * Формат хранения: ГГГГ-ММ-ДД ЧЧ:ММ:СС
     * 
     * @var int 
     */
    public $editDate=NULL;
    
    /**
     * Текст комментария
     * 
     * @var string 
     */
    public $comment=NULL;
    
    /**
     * id статьи к которой относится комментарий
     * 
     * @var int 
     */
    public $articleId=NULL;
    
    /**
     * $deleted Комментарий удален
     * 
     * @var int 
     */
    public $deleted=NULL;
    
    
    /**
     * Принимает массив данных о комментарии и сохраняет его
     * 
     * @param object $db объект базы данных
     * @param array $comment массив с данными комментария
     * 
     * @return void|string или описание ошибки
     */
    public function __construct($db=NULL, $comment=NULL) {
        
        /*
         * Вызвать конструктор родительского класса для проверки
         * существования объекта базы данных
         */
        parent::__construct($db);
        
        
        /*
         * Установить время. Далее в зависимости какой метод используется
         *  будет выбираться какое время записывать в БД
         */
        if (is_array($comment)) {
            $this->editDate=$comment['editDate'];  
            $this->publicationDate=$comment['publicationDate'];
        } else {
            $this->editDate=time();
            $this->publicationDate=time();
        }
        
        
        
        /*
         * Инициализировать свойства класса данными из массива
         */            
        if (is_array($comment)) {     //определяет, является ли переменная массивом.            
            $this->author=$comment['author'];
            $this->comment=$comment['comment'];
            $this->articleId=$comment['articleId'];
            $this->id=$comment['id']; 
            $this->deleted=$comment['deleted']; 
            
        } elseif (isset($_POST['action']) && isset($_POST['action'])) {
            $this->processForm();
            
        } else {    
            throw new Exception("Class Comment. Construct. Не были предоставлены данные о комментарии.");
        }
        
    }   //__construct
    
    
    /**
     * Добавляет комментарий текущего объекта в базу данных
     * 
     * @return int|string TRUE или описание ошибки 
     */
    public function addComment() {
        
        /*
         * Проверяем разрешения пользователя. Добавление комментария.
         */
        if (!Role::hasPrivilege($_SESSION['user']['id'], 'add comment')) {
            return "Недостаточно прав для добавления комментария";
        }
        
        
        /*
         * Сохраняем комментарий в БД
         * Используем функцию MySQL FROM_UNIXTIME() для 
         * конвертации даты публикации в формат MySQL
         */
        $sql = "INSERT INTO comments (publicationDate, comment, "
            . "author, articleId)"
            . " VALUES (FROM_UNIXTIME(:publicationDate), :comment, "
            . ":author, :articleId)";
        
        
        try {
            $stmt=$this->db->prepare($sql);
            $stmt->bindParam(":publicationDate", $this->publicationDate, PDO::PARAM_INT);
            $stmt->bindParam(":comment", $this->comment, PDO::PARAM_STR); 
            $stmt->bindParam(":author", $this->author, PDO::PARAM_STR);
            $stmt->bindParam(":articleId", $this->articleId, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->closeCursor();   
        } catch (Exception $e) {
            die ($e->getMessage());
        } 
        
        
        /*
         * После выполнения запроса метод сохраняет ID новой статьи 
         * в свойстве id с помощью функции PDO lastInsertId()
         */
        $this->id=$this->db->lastInsertId();
        
        
        return TRUE;

    }   //addComment
    
    
    
    /**
     * Обновляет комментарий текущего объекта в базе данных
     * 
     * @return int|string TRUE или описание ошибки 
     */
    public function editComment() {
        
        print "editComment. Not implemented.";
        
//        else {
//            $sql = "UPDATE comments SET comment=:comment,"
//                . " editDate=FROM_UNIXTIME(:editDate), author=:author, articleId=:articleId"
//                . " WHERE id = :id";
//        }
//        
//        else {
//                $stmt->bindParam(":id", $this->id, PDO::PARAM_INT); 
//                $stmt->bindParam(":editDate", $this->editDate, PDO::PARAM_INT);
//            }

    }   //editComment
    
    
    /**
     * Удаляет комментарий текущего объекта в базе данных
     * 
     * @return int|string TRUE или описание ошибки 
     */
    public function deleteComment() {
        
        /*
         * Если ID не был передан-ошибка
         */
        $this->id=filter_input(INPUT_POST, 'commentID', FILTER_SANITIZE_NUMBER_INT);
       
        //Сначала проверяем, что объект имеет установленноое свойство $id. 
        if (is_null ($this->id)) {
            trigger_error ("Comment::delete(): Attempt to delete an Comment "
                    . "object that that does not have its ID property set ",
                      E_USER_ERROR);
            
        } else if ($this->deleted===TRUE) {
            trigger_error ("Comment::delete(): Attempt to delete an "
                    . "deleted Comment",
                      E_USER_ERROR);
        }
        
        /*
         * Проверяем разрешения пользователя. Удаление комментария.
         */
        $deletePerm=Role::hasPrivilege($_SESSION['user']['id'], 'delete any comment');
        if ($deletePerm===false)  {
            $deletePerm=Role::hasPrivilege($_SESSION['user']['id'], 'delete own comment');
            if ($deletePerm===false) {
                return "Недостаточно прав для удаления комментария";
            }
        }
        
        /*
         * Узнаем кто автор комментария
         */
        $author=$this->getAuthorOfComment();
        
        
        /*
         * Проверяем, что пользователь удаляет свой комментарий, а не чужую.
         */
        if ($author!==$_SESSION['user']['name']) {
            return "Недостаточно прав для удаления комментария";
        }       
        
        /*
         * Удалить комментарий
         */
        $this->deleteCommentFromDB();
         
        return TRUE;
        
    }   //deleteComment
    
    
    /**
     * Возвращает все или диапазон объектов "комментарий" в базе данных
     * 
     * @param int $start С какого комментария начать извлечение из БД
     * @param int $numRows Кол-во строк (по умолчанию все)
     * @param string $order Столбец по которому производится сортировка статей 
     * (по умолчанию publicationDate DESC)
     * @param int $articleId id статьи, к которой требуются комментарии из БД
     * 
     * @return array Двухэлементный массив: results=>массив объектов  
     * "комментарий"; totalRows-общее кол-во комментариев
     */
    public static function getByArticle ($articleId, $db, $start=0, $numRows = 1000, $order = "publicationDate ASC") {
        
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS * , UNIX_TIMESTAMP (publicationDate) "
                . "AS publicationDate FROM comments WHERE articleId = :articleId ORDER BY "
                . mysql_escape_string($order)." LIMIT :startRow, :numRows";

        
        try {
            $stmt=$db->prepare($sql);
            $stmt->bindParam(':articleId', $articleId, PDO::PARAM_INT);
            $stmt->bindParam(':startRow', $start, PDO::PARAM_INT);
            $stmt->bindParam(':numRows', $numRows, PDO::PARAM_INT);
            
            $stmt->execute();
            
            $list=array();
            
            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
                $list[]=$comment=new Comment("", $row);
                //$list[]=$comment;
            }
            
            $stmt->closeCursor();
            
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        //Получаем общее кол-во статей, которые соответствуют критерию
        $sql = "SELECT FOUND_ROWS() AS totalRows";
	$totalRows = $db->query( $sql )->fetch(PDO::FETCH_ASSOC);

        return (array ("results"=>$list, "totalRows"=>$totalRows));
        
        
    }   //getByArticle
    
    
    
    /**
     * Возвращает все или диапазон объектов "комментарий" в базе данных
     * 
     * @param int $id id комментария
     * 
     * @return объект "комментарий"
     */
    public static function getByID ($db, $id) {
        
        $sql = "SELECT *, UNIX_TIMESTAMP(publicationDate) AS `publicationDate`, UNIX_TIMESTAMP(editDate) AS `editDate` FROM `comments` WHERE `id` = :id LIMIT 1";
        
        try {
            //Сразу после сохранения выражения SELECT в строке, мы подготавливаем его с помощью функции $conn->prepare(), сохраняя полученный дескриптор в переменной  $st.
            //Подготовка выражения используется для работы со многими базами данных. Она позволяет выполнять запросы быстрее и безопаснее.
            $stmt=$db->prepare($sql);
            
            //Затем мы привязываем значение переменной $id ( ID нужной статьи) к указателю места размещения :id с помощью вызова метода bindValue().
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            
            //И вызываем метод execute() для выполнения запроса.
            $stmt->execute();
            
            //После чего используем метод fetch() для перемещения полученной 
            //записи в ассоциированный массив с именами полей и соответствующими значениями, 
            //который хранится в переменной $article.
            $comment=$stmt->fetch(PDO::FETCH_ASSOC);    
            
            //closeCursor() освобождает соединение с сервером, давая возможность
            // запускать другие SQL запросы. Метод оставляет запрос в состоянии готовности к повторному запуску.
            $stmt->closeCursor();
            
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        if ($comment) {
            return new Comment ("", $comment);
            
        }

    }   //getByAuthor
    
    /**
     * Возвращает все или диапазон объектов "комментарий" в базе данных
     * 
     * @param int $start С какого комментария начать извлечение из БД
     * @param int $numRows Кол-во строк (по умолчанию все)
     * @param string $order Столбец по которому производится сортировка статей 
     * (по умолчанию publicationDate DESC)
     * @param int $author автор комментарии которого требуются из БД
     * 
     * @return array массив объектов "комментарий"
     */
    public static function getByAuthor($start=NULL, $numRows=1000, 
            $order="publicationDate DESC", $author) {
        
        

    }   //getByID
    
    
    /**
     * Предназначен для проверки формы и сохранения или
     * редактирования комментария
     * 
     * @return mixed TRUE в случае успешного завершения или сообщение об ошибке в случае сбоя
     */
    private function processForm () {
        
        /*
         * Выход, если значение "action" задано неправильно
         */
        if ($_POST['action']!='addComment') {
            return "Некорректная попытка вызова метода processForm";
        }
        
        
        /*
         * Отфильтровать данные из массива POST
         */
        $this->author=filter_input(INPUT_POST, 'userName', FILTER_SANITIZE_SPECIAL_CHARS);
        $this->comment=filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_SPECIAL_CHARS);
        $this->articleId=filter_input(INPUT_POST, 'articleID', FILTER_SANITIZE_NUMBER_INT);
        
        
        /*
         * Если id не передано значит новый комментарий
         */
//        if (empty($_POST['comment_id'])) {
//            $this->id=NULL;
//        } else {
//            $this->id=filter_input(INPUT_POST, 'comment_id', FILTER_SANITIZE_NUMBER_INT);   
//        }
        
        $this->id=NULL;
        $this->deleted=NULL;
        
        return TRUE;

    }   //processForm
    
    /**
     * Удаляет комментарий из БД
     * 
     * @return void|string Ничего или описание ошибки
     */
    private function deleteCommentFromDB () {
        //$sql = "DELETE FROM comments WHERE id =:id LIMIT 1";
        
        
        $sql = "UPDATE comments SET comment=:comment,"
            ." editDate=FROM_UNIXTIME(:editDate),"
            ." deleted=:deleted"    
            ." WHERE id = :id";
        
        $this->deleted=TRUE;
        $delString="Комментарий удален.";
            
        
        try {
            $stmt=$this->db->prepare($sql);
            
            $stmt->bindParam(":id", $this->id, PDO::PARAM_INT);
            $stmt->bindParam(":comment", $delString, PDO::PARAM_INT);
            $stmt->bindParam(":editDate", $this->editDate, PDO::PARAM_INT);
            $stmt->bindParam(":deleted", $this->deleted, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->closeCursor();
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }   //deleteCommentFromDB
    
    
    
    /**
     * Узнаем по id комментария кто автор комментария.
     * 
     * @return string автор комментария или описание ошибки
     */
    public function getAuthorOfComment () {
        $sql = "SELECT `author` FROM `comments` WHERE `id` = :id LIMIT 1";
        try {
            $stmt=$this->db->prepare($sql);
            $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            $author=$stmt->fetch(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        return $author['author'];
    }   //getAuthorOfCommentFromDB
    
    
    /**
     * Заменяет bbcode на html-теги
     * 
     * @param string $text_post Текст с bbcode
     * 
     * @return string Текст в котором bbcode заменен на html-теги
     */
    public function replaceBBCode($text_post) {
        $str_search = array(
        "#&\#13;&\#10;#is",
        "#\[b\](.+?)\[\/b\]#is",
        "#\[i\](.+?)\[\/i\]#is",
        "#\[u\](.+?)\[\/u\]#is",
        "#\[s\](.+?)\[\/s\]#is",
        "#\[code\](.+?)\[\/code\]#is",
        "#\[quote\](.+?)\[\/quote\]#is",
        "#\[url=(.+?)\](.+?)\[\/url\]#is",
        "#\[url\](.+?)\[\/url\]#is",
        "#\[img\](.+?)\[\/img\]#is",
        "#\[size=(.+?)\](.+?)\[\/size\]#is",
        "#\[color=(.+?)\](.+?)\[\/color\]#is",
        "#\[list\](.+?)\[\/list\]#is",
        "#\[listn](.+?)\[\/listn\]#is",
        "#\[\*\](.+?)\[\/\*\]#",
        "#\[video\](.+?)\[\/video\]#is"

        );
        $str_replace = array(
        "<br />",
        "<b>\\1</b>",
        "<i>\\1</i>",
        "<span style='text-decoration:underline'>\\1</span>",
        "<span style='text-decoration:line-through'>\\1</span>",
        "<code class='code'>\\1</code>",
        "<blockquote><span>Цитата:</span> <hr/>\\1<hr/></blockquote>",
        "<a href='\\1'>\\2</a>",
        "<a href='\\1'>\\1</a>",
        "<img align='left' src=\\1 alt='Изображение'/>",
        "<span style='font-size:\\1%'>\\2</span>",
        "<span style='color:\\1'>\\2</span>",
        "<ul>\\1</ul>",
        "<ol>\\1</ol>",
        "<li>\\1</li>",
        //"<video controls='controls' src=\\1></video>"
        '<iframe align="left" width="420" height="315" src=//\\1 frameborder="0" allowfullscreen></iframe>'

        );
    
        return preg_replace($str_search, $str_replace, $text_post);
        
    } //replaceBBCode
    
    
    /*
     * Обрезает строку до нужного кол-ва символов.
     * Убирает теги.
     */
    public function get_cut_text($text,$length) {
        if (strlen($text)>$length) {
            //Удаляет HTML и PHP тэги из строки
            $text = strip_tags($text);
            
            //возвращает подстроку строки string длиной length, начинающегося с start символа по счету.
            $substring_limited = substr($text,0,$length); 
            //Возвращает позицию последнего вхождения символа
            return substr($substring_limited, 0, strrpos($substring_limited, ' ' )).'...';
        } else {
            return strip_tags($text);
        }
    }
    
    
    
    
    
    
    
}   //class Comment
