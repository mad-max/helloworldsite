<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Различные вспомогательные функции для вытаскивания из БД небольшой информации
 * неподходящей к другим классам
 *
 * @author Max
 */
class ext_db extends DB_Connect {

    public function __construct($db = NULL) {
        parent::__construct($db);
    }   //__construct
    
    
    /*
     * Возвращает массив с массивами ID и названий категорий
     */
    public static function getCategoryList () {
        /*
         * Получить список категорий.
         */
        $dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
        try {
            $conn=new PDO($dsn, DB_USER, DB_PASS);
        } catch (Exception $e) {
            //Если не удается установить соединение с БД, вывести сообщение об ошибке
            die ($e->getMessage() );
        }
        $sql = "SELECT * FROM `category`";
        try {
            $stmt=$conn->prepare($sql);
            $stmt->execute();
            $listCategory=array();
            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
                $listCategory[]=$row;
            }
            $stmt->closeCursor();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }

        return $listCategory;
    }   //getCategoryList
    
    
    /*
     * Возвращает массив с ID категории и названием категории
     */
    public static function getActiveCategory ($articleID) {
        /*
         * Получить категорию текущей статьи
         */
            $dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
        try {
            $conn=new PDO($dsn, DB_USER, DB_PASS);
        } catch (Exception $e) {
            //Если не удается установить соединение с БД, вывести сообщение об ошибке
            die ($e->getMessage() );
        }
        $sql = "SELECT * FROM `category` WHERE `id_category` = :id LIMIT 1";
        try {
            $stmt=$conn->prepare($sql);
            $stmt->bindParam(':id', $articleID, PDO::PARAM_INT);
            $stmt->execute();
            $activeCategory=$stmt->fetch(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        return $activeCategory;
    }   //getActiveCategory
    
    
    
    /*
     * Возвращает массив с id категории и названием категории
     */
    public static function getIdCategoryByName ($categoryName=NULL) {
        /*
         * Получить категорию текущей статьи
         */
            $dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
        try {
            $conn=new PDO($dsn, DB_USER, DB_PASS);
        } catch (Exception $e) {
            //Если не удается установить соединение с БД, вывести сообщение об ошибке
            die ($e->getMessage() );
        }
        $sql = "SELECT * FROM `category` WHERE `name_category` = :name LIMIT 1";
        try {
            $stmt=$conn->prepare($sql);
            $stmt->bindParam(':name', $categoryName, PDO::PARAM_INT);
            $stmt->execute();
            $nameCategory=$stmt->fetch(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        return $nameCategory;
    }   //getIdCategoryByName
    
    
    
    
}   //ext_db
