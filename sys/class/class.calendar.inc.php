<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Обеспечивает создание календаря и манипулирование событиями
 * 
 * Лицензия: на этот файл распространяется лицензия MIT,
 * доступная по адресу:
 * http://www.opensource.org/licenses/mit-license.html
 * 
 * @author Jason Lengstorf <jason.lengstorf@ennuidesign.com>
 * @copyright (c) 2009, Ennui Design
 * @license http://www.opensource.org/licenses/mit-license.html
 */
class Calendar extends DB_Connect {
    /**
     * Дата, на основании которой должен строиться календарь
     * 
     * Формат хранения: ГГГГ-ММ-ДД ЧЧ:ММ:СС
     * 
     * @var string дата, выбранная для построения календаря
     */
    private $_useDate;
    
    /**
     * Месяц, для которого строится календарь
     * 
     * @var int выбранный месяц
     */
    private $_m;
    
    /**
     * Год, из которого выбирается начальный день месяца
     * 
     * @var int выбранный год
     */
    private $_y;
    
    /**
     * Количество дней в выбранном месяце
     * 
     * @var int количество дней в месяце
     */
    private $_daysInMonth;
    
    /**
     * Индекс дня недели, с которого начинается месяц (0-6)
     * 
     * @var int день недели, с которого начинается месяц
     */
    private $_startDay;
    
    //Сюда будут помещены методы
    /**
     * Создает объект базы данных и хранит соответствующие данные
     * 
     * При создании экземпляра этого класса контруктор принимает
     * объект базы данных в качестве параметра. Если значение этого параметра 
     * отлично от null, оно сохраняется в закрытом свойстве $db. Если же это
     *  значение равно null, то вместо этого создается и сохраняется 
     * новый PDO-объкт.
     * 
     * Этот метод осуществляет сбор и сохранение дополнительной информации,
     *  включая месяц, начиная с которого должен строиться календарь,
     * количество дней в указанном месяце, начальный день недели этого 
     * месяца, а также текущий день недели.
     * 
     * @param object $dbo объект базы данных
     * @param string $useDate дата, выбранная для построения календаря
     * @return void Description
     */
    public function __construct($dbo = NULL, $useDate=NULL) {
        /**
         * Вызвать конструктор родительского класса для проверки
         * существования объекта базы данных
         */
        parent::__construct($dbo);
        
        /*
         * Собрать и сохранить информацию, относящуюся к месяцу
         */
        if (isset($useDate)) {
            $this->_useDate=$useDate;
        } else {
            $this->_useDate=date('Y-m-d H:i:s');    //форматирует системную дату/время
        }
        
        /*Преобразовать во временную метку UNIX, а затем
         * определить месяц и год, которые следует использовать
         * при построении календаря
         */
        $ts=strtotime($this->_useDate); //Преобразует текстовое представление даты на английском языке в метку времени Unix 
        $this->_m=date('m', $ts);   //рассчитываем месяц на основании $ts
        $this->_y=date('Y', $ts);   //рассчитываем год на основании $ts
        
        /*
         * Определить количество дней, содержащихся в месяце
         */
        $this->_daysInMonth=  cal_days_in_month(CAL_GREGORIAN, $this->_m, $this->_y);   // возвращает количество дней в месяце для данного года и календаря.
        
        /*
         * Определить, с какого дня недели начинается месяц
         */
        $ts=  mktime(0, 0, 0, $this->_m, 1, $this->_y); //Возвращает метку времени для заданной даты
        $this->_startDay=  date('w', $ts);
        if ($this->_startDay===0) {
            $this->_startDay=6;
        } else {
            --$this->_startDay;
        }
    }   //__construct
    
    
    /**
     * Загружает информацию о событии (событиях) в массив
     * 
     * @param int $id необязательный идентификатор (ID),
     * используемый для фильтрации результатов
     * @return array массив событий, извлеченных из базы данных
     */
    private function _loadEventData ($id=NULL) {
        $sql="SELECT
            `event_id`, `event_title`, `event_desc`, 
            `event_start`, `event_end`
            FROM `events` ";
        /*
         * Если предоставлен идентификатор (ID) события, добавить 
         * предложение WHERE, чтобы было возвращено только это событие.
         * В противном случае загрузить все события, относящиеся к используемому месяцу
         */
        if (!empty($id)) {
            $sql.="WHERE `event_id`=$id LIMIT 1 ";
        } else {
            /*
             * Найти первый и последний дни месяца
             */
            $start_ts=  mktime(0, 0, 0, $this->_m, 1, $this->_y);   //первый день месяца
            $end_ts=mktime(23, 59, 59, $this->_m+1, 0< $this->_y);  //последний день месяца ("нулевой" день следующего месяца)
            $start_date=date('Y-m-d H:i:s', $start_ts);
            $end_date=date('Y-m-d H:i:s', $end_ts);
        
        /*
         * Отфильтровать события, выбрав из них только те,
         * которые относятся к месяцу, выбранному текущим
         */
        $sql.="WHERE `event_start` BETWEEN '$start_date' AND '$end_date' ORDER BY `event_start` ";
        }
        
        try {
            $stmt=$this->db->prepare($sql);
            /*
             * Привязать параметр, если был передан идентификатор
             */
            if (!empty($id)) {
                $stmt->bindParam(";id", $id, PDO::PARAM_INT);
            }
            $stmt->execute();
            $results=$stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            return $results;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }   //_loadEventData
    
    /**
     * Загружает все события, относящиес к данному месяцу, в массив
     * 
     * @return array информация о событиях
     */
    private function _createEventObj() {
        /*
         * загрузить массив событий
         */
        $arr=$this->_loadEventData();
        
        /*
         * Создать новый массив, а затем организовать события в 
         * зависимости от дня месяца, в который они происходят
         */
        $events=array();
        foreach ($arr as $event) {
            $day=date('j', strtotime($event['event_start']));
            try {
                $events[$day][]=new Event($event);
            } catch (Exception $e) {
                die ($e->getMessage());
            }
        }
        return $events;
        
    }   //_createEventObj
    
    /**
     * Возвращает объект одиночного события
     * 
     *@param int $id идентификатор (ID)события
     * @return object объект события
     */
    private function _loadEventById($id) {
        /*
         * Если ID не передан, возвратить NULL
         */
        if (empty($id)) {
            return NULL;
        }
        /*
         * Загрузить события в массив
         */
        $event=$this->_loadEventData($id);
        
        /*
         * Возвратить объект события
         */
        if (isset($event[0])) {
            return new Event($event[0]);
        } else {
            return NULL;
        }
        
    }   //_loadEventById
    
    /**
     * Генерирует разметку для отображения административных ссылок
     * 
     *@return string разметка для отображения административных ссылок
     */
    private function _adminGeneralOptions() {
        /*
         * Если пользователь выполнил процедуру входа,
         * отобразить средства администрирования
         */
        if (isset($_SESSION['user'])) {
            /*
             * Отобразить административные элементы управления
             */
            return <<<ADMIN_OPTIONS
            <a href="admin.php" class="admin">+ Добавить новое событие</a>
            <form action="assets/inc/process.inc.php" method="post">
                <div>
                    <input type="submit" value="Выход"  />
                    <input type="hidden" name="token"
                       value="$_SESSION[token]" />
                    <input type="hidden" name="action"
                        value="user_logout" />
                </div>
            </form>
ADMIN_OPTIONS;
        } else {
            return <<<ADMIN_OPTIONS
            <a href="./login.php">Log In</a>
ADMIN_OPTIONS;
        }
    }      //_adminGeneralOptions
    
    /**
     * Генерирует опции редактирования и удаления для события с заданным идентификатором (ID)
     * 
     * @param int $id идентификатор события, для которого генерируются опции
     * @return string разметка для опций редактирования/удаления
     */
    private function _adminEntryOptions($id) {
        if (isset($_SESSION['user'])) {
            return <<<ADMIN_OPTIONS
            <div class="admin-options">
            <form action="admin.php" method="post">
                <p>
                    <input type="submit" name="edit_event"
                        value="Редактировать событие" />
                    <input type="hidden" name="event_id"
                        value="$id" />
                </p>
            </form>
            <form action="confirmDelete.php" method="post">
                <p>
                    <input type="submit" name="delete_event"
                        value="Удалить событие" />
                    <input type="hidden" name="event_id"
                        value="$id" />
                </p>
            </form>
            </div>  <!--end admin options-->
ADMIN_OPTIONS;
        } else {
            return NULL;
        }
    }   //_adminEntryOptions

    
    /**
     * Возвращает HTML-разметку для отображения календаря и событий
     * 
     * На основании информации, хранящейся в свойствах класса,
     * загружаются события для данного месяца, генерируется
     * календарь и возвращается актуальная разметка.
     * 
     * @return string HTML-разметка календаря
     */
    public function buildCalendar() {
        /*
         * Определить месяц календаря и создать массив сокращенных
         * обозначений дней недели, которые будут использованы
         * в заголовках столбцов
         */
        $cal_month=date('F Y', strtotime($this->_useDate)); //F-Полное наименование месяца. Y-Порядковый номер года, 4 цифры. strtotime-преобразует текстовое представление даты на английском языке в метку времени Unix 
        $cal_id=date('Y-m', strtotime($this->_useDate));
        $weekdays=array('Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс');
        
        /*
         * Добавить заголовок в HTML-разметку календаря
         */
        $html="\n\t<h2 id=\"month-$cal_id\">$cal_month</h2>";   //использование префикса month означает, что вы придерживаетесь стандартов W3, согласно которым идентификаторы элементов должны начинаться с буквы.
        for ($d=0, $labels=NULL; $d<7; ++$d) {
            $labels.="\n\t\t<li>".$weekdays[$d]."</li>";
        }
        $html.="\n\t<ul class=\"weekdays\">".$labels."\n\t</ul>";
        
        /*
         * Загрузить данные о событиях
         */
        $events=  $this->_createEventObj();
        
        
        
        /*
         * Создать HTML-разметку календаря
         */
        $html.="\n\n<ul>";  //Начать новый неупорядоченный список
        for ($i=1, $c=1, $t=date('j'), $m=date('m'), $y=date('Y'); $c<=$this->_daysInMonth; ++$i) {
            /*
             * Применить класс "fill" к ячейкам календаря,
             * располагающимися перед первым днем данного месяца
             */
            $class=$i<=$this->_startDay ? "fill" : NULL;    //Выражение (expr1) ? (expr2) : (expr3) интерпретируется как expr2, если expr1 вычисляется в TRUE, или как expr3 если expr1 вычисляется в FALSE.
            
            /*
             * Добавить класс "today", если дата совпадает с текущей
             */
            if ($c==$t && $m==$this->_m && $y==$this->_y && $i>$this->_startDay) {
                $class="today";
            }
            
            /*
             * Создать открывающий и закрывающий дескрипторы элемента списка
             */
            $ls=sprintf("\n\t\t<li class=\"%s\">", $class);
            $le="\n\t\t</li>";
            
            /*
             * Добавить день месяца, идентифицирующий ячейку календаря
             */
            if ($this->_startDay<$i && $this->_daysInMonth>=$c) {   //
                /*
                 * Форматировать данные о событиях
                 */
                $event_info=NULL; //clear the variable
                if (isset($events[$c])) {
                    foreach ($events[$c] as $event) {
                    $link='<a href="view.php?event_id=' //через знак вопроса присоединяется get запрос
                            .$event->id .'">'.$event->title.'</a>';
                    $event_info.="\n\t\t\t$link";
                    }
                }
                $date=sprintf ("\n\t\t\t%02d", $c++);
                
            } else {
                $date="&nbsp;"; //&nbsp - неразделимый пробел
            }
            
            /*
             * Если текущий день воскресенье, перейти в следующий ряд
             */
            $wrap=$i!=0 && $i%7==0 ? "\n\t</ul>\n\t<ul>" : NULL;
            
            /*
             * Собрать разрозненные части воедино
             */
            $html.=$ls.$date.$event_info.$le.$wrap;
        }   //end for
        
        /*
         * Добавить заполнители для завершения последней недели
         */
        
        while ($i%7!=1) {
            $html.="\n\t\t<li class=\"fill\">&nbsp;</li>"; ++$i;
        }
        
        /*
         * Закрыть окончательный неупорядоченный список
         */
        $html.="\n\t</ul>\n\n";
        
        /*
         * Если выполнен вход, отобразить опции администрирования
         */
        //$admin=$this->_adminGeneralOptions();
        
        /*
         * Возвратить HTML-разметку для вывода
         */
        return $html;//.$admin;
        
    }   //buildCalendar
    
    /**
     * Отображает информацию о заданном событии
     * 
     * @param int $id идентификатор ID события
     * @return string элементарная разметка для отображения информации о событии
     */
    public function displayEvent($id) {
        /*
         * Убедиться в том, что ID был передан
         */
        if (empty($id)) {return NULL;}
        
        /*
         * Проверить, что ID является целым числом
         */
        $id=preg_replace('/[^0-9]/','', $id);
        
        /*
         * Загрузить данные о событии из БД
         */
        $event=$this->_loadEventById($id);
        
        /*
         * Сгенерировать строки для даты, начального и конечного времени
         */
        $ts=  strtotime($event->start);
        $date=date('F d, Y', $ts);
        $start=date('g:ia', $ts);
        $end=date('g:ia', strtotime($event->end));
        
        /*
         * Загрузить административные опции, если пользователь выполнил вход
         */
        $admin=$this->_adminEntryOptions($id);
        
        /*
         * Сгенерировать и возвратить разметку
         */
        return "<h2>$event->title</h2>"
                ."\n\t<p class=\"dates\">$date, $start&mdash;$end</p>"
                ."\n\t<p>$event->description</p>$admin";
        
    }   //displayEvent
    
    /**
     * Генерирует форму, позволяющую редактировать или
     * создавать события
     * 
     * @return string HTML-разметка формы для редактирования событий
     */
    public function displayForm() {
        /*
         * Проверить, был ли передан идентификатор (ID)
         */
        if (isset($_POST['event_id'])) {
            $id=(int) $_POST['event_id'];
                //Принудительно задать целочисленный тип для
                //обеспечения безопасности данных
        } else {
            $id=NULL;
        }
        
        /*
         * Инициализировать переменную, хранящую текст заголовка и надписи на кнопке отправки формы
         */
        $submit="Создать событие";
        
        /*
         * Если передан ID, загрузить соответствующее событие
         */
        if (!empty($id)) {
            $event=$this->_loadEventById($id);
            
            /*
             * Если не возвращен объект, возвратить NULL
             */
            if (!is_object($event)) {return NULL;}  //определяет, является ли переменная объектом.
            $submit="Изменить событие";
        }
        
        /*
         * Создать разметку
         */
        return <<<FORM_MARKUP
        
        <form action="assets/inc/process.inc.php" method="post">
            <fieldset>
                <legend>$submit</legend>
                <label for="even_title">Название события</label>
                <input type="text" name="event_title"
                    id="event_title" value="$event->title" />
                <label for="event_start">Время начала</label>
                <input type="text" name="event_start"
                    id="event_start" value="$event->start" />
                <label for="event_end">Время окончания</label>
                <input type="text" name="event_end"
                    id="event_end" value="$event->end" />
                <label for="event_description">Описание события</label>
                <textarea name="event_description"
                    id="event_description">$event->description
                </textarea>
                <input type="hidden" name="event_id" value="$event->id" />
                <input type="hidden" name="token" value="$_SESSION[token]" />
                <input type="hidden" name="action" value="event_edit" />
                <input type="submit" name="event_submit" value="$submit" />
                или <a href="./index.php">Отменить</a>
               </fieldset>
            </form>
FORM_MARKUP;
                
    }   //displayForm
    
    
    /**
     * Предназначен для проверки формы и сохранения или
     * редактирования события
     * 
     * @return mixed TRUE в случае успешного завершения или сообщение об ошибке в случае сбоя
     */
    public function processForm() {
        /*
         * Выход, если значение "action" задано неправильно
         */
        if ($_POST['action']!='event_edit') {
            return "Некорректная попытка вызова метода processForm";
        }
        
        /*
         * Извлечь данные из формы
         */
        $title=htmlentities($_POST['event_title'], ENT_QUOTES, "UTF-8"); //Преобразует символы в соответствующие HTML сущности. 
        $desc=  htmlentities($_POST['event_description'], ENT_QUOTES, "UTF-8");
        $start=  htmlentities($_POST['event_start'], ENT_QUOTES, "UTF-8");
        $end=  htmlentities($_POST['event_end'], ENT_QUOTES, "UTF-8");
        
        /*
         * Если ID не был передан, создать новое событие
         */
        if (empty($_POST['event_id'])) {
            $sql="INSERT INTO `events` (`event_title`, `event_desc`, `event_start`, `event_end`) "
                    . "VALUES (:title, :description, :start, :end)";
        } else {
            /*
            * Обновить событие, если оно редактировалось
            */
            $id=(int) $_POST['event_id'];   //привести ID события к целочисленному типу в интересах безопасности
            $sql="UPDATE `events` "
                    . "SET "
                    . "`event_title`=:title, "
                    . "`event_desc`=:description, "
                    . "`event_start`=:start, "
                    . "`event_end`=:end "
                    . "WHERE `event_id`=$id";
        }
        
        /*
         * После привязки данных выполнить запрос создания или редактирования события
         */
        try {
            $stmt=$this->db->prepare($sql);
            $stmt->bindParam(":title", $title, PDO::PARAM_STR); //Связывает параметр с указанным именем переменной
            $stmt->bindParam(":description", $desc, PDO::PARAM_STR);
            $stmt->bindParam(":start", $start, PDO::PARAM_STR);
            $stmt->bindParam(":end", $end, PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();
            
            /*
             * Возвратить ID события
             */
            return $this->db->lastInsertId();
                   
        } catch (Exception $e) {
            return $e->getMessage();

        }        
    }   //processForm
    
    /**
     * Получает подтверждение необходимости удаления события
     * и, если это действительно требуется, выполняет операцию.
     * 
     * После выполнения пользователем щелчка на кнопке удаления
     * генерирует диалоговое окно подтверждения. При получении
     * подтверждения удаляет событие из базы данных и отправляет
     * пользователя обратно в основное представление календаря. Если
     * передумал удалять событие, он возвращается назад в основное представление
     *  календаря, и удаления события не происходит.
     * 
     * @param int $id идентификатор ID события
     * @return mixed форма в случае получения подтверждения и ничего или 
     * сообщение об ошибке в случае удаления события
     */
    public function confirmDelete($id) {
        /*
         * Убедиться в том, что идентификатор (ID) был предан
         */
        if (empty($id)) {return NULL;}
        
        /*
         * Убедиться в том, что идентификатор является целым числом
         */
        $id=  preg_replace('/[^0-9]/', '', $id);
        
        /*
         * Если была отправлена форма подтверждения, снабженная
         * действительным маркером, проверить данные, переданные
         * вместе с формой
         */
        if (isset($_POST['confirm_delete']) && $_POST['token']==$_SESSION['token']) {
            /*
             * Если удаление подверждено пользователем, удалить событие
             * из базы данных
             */
            if ($_POST['confirm_delete']=="Да, удалить") {
                $sql="DELETE FROM `events` "
                        . "WHERE `event_id`=:id "
                        . "LIMIT 1 ";
                try {
                    $stmt=$this->db->prepare($sql);
                    $stmt->bindParam (
                            ":id",
                            $id,
                            PDO::PARAM_INT
                            );
                    $stmt->execute();
                    $stmt->closeCursor();
                    header("Location: ./");
                    return;
                } catch (Exception $ex) {
                    return $ex->getMessage();
                }
            }
            
            /*
             * Если удаление не подтверждено пользователем, вернуть
             * его на основную страницу
             */
            else {
                header("Location: ./");
                return;
            }
        }
        
        /*
         * Если форма для подтверждения не была передана,
         * отобразить ее
         */
        $event=$this->_loadEventById($id);
        
        /*
         * Если не был возвращен объект, вернуться в основное представление
         */
        if (!is_object($event)) { header("Location: ./"); }
        
        return <<<CONFIRM_DELETE
        <form action="confirmDelete.php" method="post">
        <h2>
            Вы действительно хотите удалить событие "event->title"?
        </h2>
        <p><strong>Удаленное событие невозможно восстановить</strong></p>
        <p>
            <input type="submit" name="confirm_delete"
                value="Да, удалить" />
            <input type="submit" name="confirm_delete"
                value="Нет! Это была шутка!" />
            <input type="hidden" name="event_id"
                value="$event->id" />
            <input type="hidden" name="token"
                value="$_SESSION[token]" />
        </p>
        </form>
CONFIRM_DELETE;
    }   //confirmDelete
    
    
    
    
    
    
    
    
    
}
