<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Выводит шаблон tpl.
 *
 * @author Max
 */
class Template {
    
  private $dir_tmpl; // Директория с tpl-файлами
  private $data = array(); // Данные для вывода

  public function __construct($dir_tmpl) {
    $this->dir_tmpl = $dir_tmpl;
  }

  /* Метод для добавления новых значений в данные для вывода */
  public function set($name=NULL, $value=NULL) {
    $this->data[$name] = $value;
  }

  /* Метод для удаления значений из данных для вывода */
  public function delete($name) {
    unset($this->data[$name]);
  }

  /* При обращении, например, к $this->title будет выводиться $this->data["title"] */
  public function __get($name) {
    if (isset($this->data[$name])) { return $this->data[$name]; }
    return "";
  }

  /* Вывод tpl-файла, в который подставляются все данные для вывода */
  public function display($template) {
      
    $template = $this->dir_tmpl.$template.".tpl";
    
    //ob_start - включает буферизацию вывода.
    //Если буферизация вывода активна, никакой вывод скрипта не высылается (кроме шапок/headers);
    // вывод сохраняется во внутреннем буфере.
    ob_start();
    
    //предназначена для включения файлов в код сценария PHP во время исполнения сценария PHP.
    include ($template);
    
    //Получить содержимое буфера и очистить его
    echo ob_get_clean();
    
  } //display
  
  
}


