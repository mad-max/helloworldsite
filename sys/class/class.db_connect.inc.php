<?php


/**
 * Осуществляет подключение к базе данных
 *
 * Версия PHP 5
 * 
 * @author Max Maximov
 * @copyright (c) 2014, Max
 */
class DB_Connect {
    /**
     * Переменная для хранения объекта базы данных
     * 
     * @var object: объект базы данных
     */
    protected $db;
    /**
     * Проверить наличие объекта БД, а в случае его отсутствия создать новый
     * 
     * @param object $db объект базы данных
     */
    protected function __construct($db=NULL) {
        if (is_object($db)) {
            $this->db=$db;
        } else {
            //  Константы определены в файле
            //  db-cred.inc.php
            $dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
            try {
                $this->db=new PDO($dsn, DB_USER, DB_PASS);
            } catch (Exception $e) {
                //Если не удается установить соединение с БД, вывести сообщение об ошибке
                die ($e->getMessage() );
            }
        }   //if
    }   //_construct
}   //class DB_Connect 





















