<?php

/*
 * Данный файл init.inc.php генерирует маркер защиты от CSRF 'token',
 * загружает конфигурационную информацию из файла конфигурации,
 * подключается к базе данных.
 */

/*
 * Запуск сеанса
 */
session_start();

/*
 * Сгенерировать маркер защиты от CSRF, если это не было сделано ранее
 */
if (!isset($_SESSION['token'])) {   //определяет, установлена ли переменная.
    $_SESSION['token']=sha1(uniqid(mt_rand(), TRUE));   //генерирует уникальный id. генерирует наилучшее случайное значение.
}


/*
 * Включить необходимую конфигурационную информацию
 */
include_once '../sys/config/db_cred.inc.php';

/*
 * Определить константы для конфигурационной информации
 */
foreach ($C as $name=>$val) {
    define ($name, $val);   //определяет именованную константу.
}

/*
 * Создать PDO-объект
 */
$dsn="mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
$dbo=new PDO($dsn, DB_USER, DB_PASS);

/*
 * Определить для классов функцию автозагрузки
 */
function __autoload($class) {
    $filename="../sys/class/class.".strtolower($class).".inc.php";
    if (file_exists($filename)) {
        include_once $filename;
    }
}






